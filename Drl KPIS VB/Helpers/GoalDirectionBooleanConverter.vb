﻿Public Class GoalDirectionBooleanConverter
    Implements IMultiValueConverter


    Dim theValue As Object
    Public Function Convert(values() As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IMultiValueConverter.Convert
        Return values(0) = values(1)
        theValue = values(1)
    End Function

    Public Function ConvertBack(value As Object, targetTypes() As Type, parameter As Object, culture As Globalization.CultureInfo) As Object() Implements IMultiValueConverter.ConvertBack

    End Function
End Class
