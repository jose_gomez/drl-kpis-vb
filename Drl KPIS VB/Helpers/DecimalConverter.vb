﻿
Public Class DecimalConverter
    Implements IValueConverter


    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Return Decimal.Parse(value).ToString("F2")
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Try
            Return Decimal.Parse(value)
        Catch ex As Exception
            Return 0
        End Try
    End Function
End Class
