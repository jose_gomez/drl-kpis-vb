﻿Public Class JTSPrincipal
    Implements Security.Principal.IPrincipal

    Public ReadOnly Property Identity As Security.Principal.IIdentity Implements Security.Principal.IPrincipal.Identity
        Get
            Return UserUtility.GetLoggedOnUser()
        End Get
    End Property

    Public Function IsInRole(role As String) As Boolean Implements Security.Principal.IPrincipal.IsInRole
        Return (UserUtility.GetLoggedOnUser().Role = role)
    End Function

    Public Shared Function IsValidUser(userName As String, password As String) As Boolean
        Dim JtsContext As New JTSDataContext(ConnectionStringsHelper.GetJTSDBConnectionString())
        Dim dbUser = (From emp In JtsContext.tblEmployees Where emp.UserName.ToLower() = userName And emp.Password = password
                    Select emp).FirstOrDefault()
        Dim result As Boolean
        If Not dbUser Is Nothing Then
            Dim user = UserUtility.GetLoggedOnUser()
            user.UserName = userName
            user.Password = dbUser.Password
            user.FirstName = dbUser.FirstName
            user.LastName = dbUser.LastName
            user.UserID = dbUser.EmployeeID
            user.Role = GetRoleByUser(dbUser.SecurityLevel)
            user.IsLoggedIn = True
            user.Picture = GetUserPicture(dbUser.EmployeeID)
            result = True
        End If
        Return result
    End Function

    Public Shared Function GetRoleByUser(securityLevel As Integer) As String
        Dim kpiDataContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim rol As String = (From r In kpiDataContext.UserRoles Where r.ID = securityLevel Select r.RoleName).FirstOrDefault()

        Return If(rol Is Nothing, String.Empty, rol)
    End Function

    Public Shared Function GetUserPicture(employeeID As Integer) As Byte()
        Dim jtsContext As New JTSDataContext(ConnectionStringsHelper.GetJTSDBConnectionString())
        Dim picture As Byte() = (From pic In jtsContext.tblFotosUsuarios Where pic.EmployeeID = employeeID Select pic.Foto).FirstOrDefault().ToArray()

        Return picture
    End Function
End Class
