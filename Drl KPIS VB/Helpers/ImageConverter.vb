﻿
Public Class GoalDirectionsImageConverter
    Implements IValueConverter


    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Select Case value

            Case 1
                Return New BitmapImage(New Uri("pack://application:,,,/Images/CheckIn_13188.png"))
            Case 2
                Return New BitmapImage(New Uri("pack://application:,,,/Images/CheckOutforEdit_13187.png"))
            Case Else
                Return Nothing
        End Select
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function
End Class
