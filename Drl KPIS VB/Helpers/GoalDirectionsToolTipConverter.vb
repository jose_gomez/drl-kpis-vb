﻿Public Class GoalDirectionsToolTipConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Select Case value

            Case 1
                Return "Valor minimo de la meta"
            Case 2
                Return "Valor Maximo de meta"
            Case Else
                Return "Valor minimo de la meta"
        End Select
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Return New NotImplementedException()
    End Function
End Class
