﻿Public Class IndicatorBoards
    Dim KPIMonthly As New MonthlyKPIBoardByMonth
    Dim WeeklyKPI As New WeeklyBigScreenBoard
    Dim WeeklyActionPlan As WeeklyActionPlanBoard
    Dim screenFormat As ScreenFormats
    Public Sub New()
    
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
       
    End Sub


    Public Sub New(currentMonth As DateTime, currentDept As Integer)

        ' This call is required by the designer.
        InitializeComponent()
        Me.KPIMonthly = Me.Resources("KPIMonthly")
     

        Me.KPIMonthly.CurrentDept = currentDept
        Me.KPIMonthly.CurrentMonth = currentMonth
        ' Add any initialization after the InitializeComponent() call.
        Me.KPIMonthly.ShowReport()

    End Sub

    Public Sub New(currentMonth As DateTime, currentDept As Integer, screen As ScreenFormats)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.screenFormat = screen
        Me.WeeklyActionPlan = Me.Resources("WeeklyActionPlan")
        Me.ctlContent.Content = Me.WeeklyActionPlan
        If Me.screenFormat = ScreenFormats.StandardScreens Then
            Me.KPIMonthly = Me.Resources("KPIMonthly")


            Me.KPIMonthly.CurrentDept = currentDept
            Me.KPIMonthly.CurrentMonth = currentMonth
            Me.ctlContent.Content = Me.KPIMonthly
            Me.KPIMonthly.ShowReport()
            Me.ctlContent.Visibility = Windows.Visibility.Visible
        Else
            Me.ctlContent.Visibility = Windows.Visibility.Hidden
            Dim semana As Integer = CalendarUtil.GetIso8601WeekOfYear(currentMonth)
            Dim firstDay As DateTime = CalendarUtil.FirstDateOfWeekISO860(currentMonth.Year, semana)
            Dim plans As New List(Of ActionPlanViewModel)
            Me.WeeklyKPI = Me.Resources("WeeklyKPI")
            Me.WeeklyKPI.Semana = semana
            Me.WeeklyKPI.Desde = firstDay
            Me.WeeklyKPI.Hasta = firstDay.AddDays(6)
            Dim items = Me.GetWeeklyKPI(currentMonth, currentDept)
            Me.WeeklyKPI.items = items
            For Each item In items
                plans.AddRange(DataLayer.GetActionPlanForDefinedGoal(item.DefinedGoalID))
            Next
            Me.WeeklyActionPlan.ActionPlans = plans
            Me.WeeklyActionPlan.Refresh()

        End If
    End Sub


    Private Function GetWeeklyKPI(theDate As DateTime, dept As Integer) As List(Of WeeklyGoalsReportItem)
        Dim kpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
     
        Dim kps = DataLayer.GetWeeklyKPIS(theDate, dept)
        Return kps
    End Function
  
End Class
