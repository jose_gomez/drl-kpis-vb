﻿Public Class AgregarCategorias
    Dim kpContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
    Private Sub wndNewCategory_Loaded(sender As Object, e As RoutedEventArgs) Handles wndNewCategory.Loaded
        Me.grdCategories.RowHeaderWidth = 0
        Me.grdCategories.CanUserAddRows = False
        Me.grdCategories.CanUserDeleteRows = False
        Me.grdCategories.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden
        BindGrid()

    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Dim newCat = (From i As Category In kpContext.Categories Where i.CategoryName = txtNuevaCategoria.Text Select i).FirstOrDefault()
        If Not newCat Is Nothing Then
            MessageBox.Show("Esta categoria ya existe. Por favor editar o borrar para agregar nueva",
                            "Nuevas Categorias", MessageBoxButton.OK, MessageBoxImage.Information)
            Exit Sub
        End If
        If Not String.IsNullOrEmpty(txtNuevaCategoria.Text) Then
            kpContext.Categories.InsertOnSubmit(New Category() With {.CategoryName = txtNuevaCategoria.Text})
            kpContext.SubmitChanges()
            BindGrid()
        Else
            MessageBox.Show("Debe de introducir el nombre de la categoria.",
                            "Nuevas Categorias", MessageBoxButton.OK, MessageBoxImage.Exclamation)

        End If
    End Sub

    Private Sub BindGrid()
        Dim categories = (From i As Category In kpContext.Categories Select i).ToList()
        Me.grdCategories.ItemsSource = categories
    End Sub

    Private Sub grdCategories_CellEditEnding(sender As Object, e As DataGridCellEditEndingEventArgs) Handles grdCategories.CellEditEnding
       
        Dim catID = CType(Me.grdCategories.SelectedItem, Category).ID
        Dim cate As Category = (From c In kpContext.Categories Where c.ID = catID Select c).FirstOrDefault()
       
        If Not cate Is Nothing Then
            If String.IsNullOrEmpty(CType(e.EditingElement, TextBox).Text) Then
                If MessageBox.Show("Esto va a borrar la categoria seleccionada. ¿Desea continuar?",
                                   "Borrar categoria", MessageBoxButton.YesNo, MessageBoxImage.Question) = MessageBoxResult.Yes Then
                    kpContext.Categories.DeleteOnSubmit(cate)
                    kpContext.SubmitChanges()
                    Exit Sub
                End If
                CType(e.EditingElement, TextBox).Text = cate.CategoryName
                Exit Sub
            End If
            cate.CategoryName = CType(e.EditingElement, TextBox).Text
            kpContext.SubmitChanges()
        End If
    End Sub


    Private Sub txtFilter_TextChanged(sender As Object, e As TextChangedEventArgs) Handles txtFilter.TextChanged
        Dim categories = (From i As Category In kpContext.Categories Where i.CategoryName.Contains(txtFilter.Text) Select i).ToList()
        Me.grdCategories.ItemsSource = categories
    End Sub
End Class
