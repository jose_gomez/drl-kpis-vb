﻿Public Class UIElementsDataHelper

    Public Shared Sub FillDepartmentsCombobox(ByRef combo As ComboBox)
        Dim kpContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim depts = (From dep In kpContext.Departments Select dep Order By dep.DepartmentName).ToList()
        combo.ItemsSource = depts
        combo.SelectedValuePath = "ID"
        combo.DisplayMemberPath = "DepartmentName"
        kpContext.Dispose()
    End Sub

    Public Shared Sub FillDepartmentsWithKPIsCombobox(ByRef combo As ComboBox)
        Dim kpContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim depts = (From dep In kpContext.Departments Join k In kpContext.KPIs On k.DepartmentID Equals dep.ID
                     Select dep Order By dep.DepartmentName).Distinct().ToList().OrderBy(Function(x) x.DepartmentName)
        combo.ItemsSource = depts
        combo.SelectedValuePath = "ID"
        combo.DisplayMemberPath = "DepartmentName"
        kpContext.Dispose()
    End Sub

    Public Shared Sub FillCategoriesCombobox(ByRef combo As ComboBox)
        Dim kpContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim cats = (From cat In kpContext.Categories Select cat Order By cat.CategoryName).ToList()
        combo.ItemsSource = cats
        combo.SelectedValuePath = "ID"
        combo.DisplayMemberPath = "CategoryName"
        kpContext.Dispose()
    End Sub

    Public Shared Sub FillKPICombobox(ByRef combo As ComboBox)
        Dim kpContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim kpis = (From kpi In kpContext.KPIs Select kpi Order By kpi.KPIName).ToList()
        combo.ItemsSource = kpis
        combo.SelectedValuePath = "ID"
        combo.DisplayMemberPath = "KPIName"
        kpContext.Dispose()
    End Sub

    Public Shared Sub FillKPIComboboxByDepartment(ByRef combo As ComboBox, deptID As Integer)
        Dim kpContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim kpis = (From kpi In kpContext.KPIs Where kpi.DepartmentID = deptID Select kpi Order By kpi.KPIName).ToList()
        combo.ItemsSource = kpis
        combo.SelectedValuePath = "ID"
        combo.DisplayMemberPath = "KPIName"
        kpContext.Dispose()
    End Sub
End Class
