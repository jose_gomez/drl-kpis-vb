﻿Imports System.Collections.ObjectModel
Public Class Agregar_Nuevo_Indicador
    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        UIElementsDataHelper.FillDepartmentsCombobox(Me.cboDepartment)
        UIElementsDataHelper.FillCategoriesCombobox(Me.cboCategory)
        BindDatagrid("")
    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Dim catID As Integer = cboCategory.SelectedValue
        Dim deptID As Integer = cboDepartment.SelectedValue
        If Not cboCategory.SelectedValue > 0 And Not cboDepartment.SelectedValue > 0 Then
            MessageBox.Show("Por favor seleccione una categoria y un departamento para este KPI.",
                                "Nuevo KPI", MessageBoxButton.OK, MessageBoxImage.Exclamation)
            Exit Sub
        End If
        If Not String.IsNullOrEmpty(txtKPI.Text) Then
            Dim kpContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
            Dim kp = (From k In kpContext.KPIs Where k.KPIName = txtKPI.Text And k.DepartmentID = deptID _
                     And k.CategoryID = catID Select k).FirstOrDefault()
            If Not kp Is Nothing Then
                MessageBox.Show("Este KPI ya existe para esta categoria y departamento. Si desea cambiarle el nombre por favor editelo buscandolo con la caja de filtro.",
                                "Nuevo KPI", MessageBoxButton.OK, MessageBoxImage.Exclamation)
                Exit Sub
            End If
            kpContext.KPIs.InsertOnSubmit(New KPI() With {.KPIName = txtKPI.Text, .DepartmentID = cboDepartment.SelectedValue, _
                                                          .CategoryID = cboCategory.SelectedValue})
            kpContext.SubmitChanges()
            MessageBox.Show("KPI Guardado", "Nuevo KPI", MessageBoxButton.OK, MessageBoxImage.Information)
            BindDatagrid("")
        Else
            MessageBox.Show("Por favor introduzca el nombre para el KPI", "Nuevo KPI", MessageBoxButton.OK, MessageBoxImage.Exclamation)
        End If
    End Sub

    Private Sub BindDatagrid(filter As String)
        Dim kpContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim kpis = (From k In kpContext.KPIs Join dep In kpContext.Departments
                    On k.DepartmentID Equals dep.ID Join cat In kpContext.Categories On k.CategoryID Equals cat.ID
                    Where k.KPIName.Contains(filter) Select k.ID, k.KPIName, dep.DepartmentName, cat.CategoryName).ToList()
        Me.grdKPI.ItemsSource = kpis
    End Sub

    Private Sub grdKPI_MouseLeftButtonUp(sender As Object, e As MouseButtonEventArgs) Handles grdKPI.MouseLeftButtonUp

    End Sub

    Private Sub txtFiltro_TextChanged(sender As Object, e As TextChangedEventArgs) Handles txtFiltro.TextChanged
        BindDatagrid(txtFiltro.Text)
    End Sub
End Class



