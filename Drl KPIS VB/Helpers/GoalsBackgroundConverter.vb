﻿Public Class GoalsBackgroundConverter
    Implements IMultiValueConverter


    Public Function Convert(values() As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IMultiValueConverter.Convert
        Dim lgb As New LinearGradientBrush
        Dim valor = values(0)
        Dim meta = values(1)
        Dim GoalDirection As GoalDirections = values(2)
        lgb.GradientStops.Add(New GradientStop(Colors.White, 1))

        If GoalDirection = GoalDirections.Top Then
            If meta > valor Then
                lgb.GradientStops.Add(New GradientStop(Colors.Red, 0))
            Else
                lgb.GradientStops.Add(New GradientStop(Colors.Green, 0))
            End If
        Else
            If valor > meta Then
                lgb.GradientStops.Add(New GradientStop(Colors.Red, 0))
            Else
                lgb.GradientStops.Add(New GradientStop(Colors.Green, 0))
            End If
        End If
        Return lgb
    End Function

    Public Function ConvertBack(value As Object, targetTypes() As Type, parameter As Object, culture As Globalization.CultureInfo) As Object() Implements IMultiValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function
End Class
