﻿Imports System.Collections.ObjectModel
Imports System.Linq
Imports System.Collections.Generic
Imports System.Runtime.CompilerServices
Public Class SeguimientoIndicadores
    Dim metas As New ObservableCollection(Of KPIsViewModel)
    Dim planes As New ObservableCollection(Of ActionPlanViewModel)
    Dim requiringAttentionResults As List(Of SP_GoalsRequiringAttentionResult)
    Dim definedGoalsIds As New List(Of Integer)
    Private Sub SeguimientoIndicadores_Loaded(sender As Object, e As RoutedEventArgs) Handles MyBase.Loaded, MyBase.Loaded
        UIElementsDataHelper.FillDepartmentsWithKPIsCombobox(Me.CboDepartamento)
        Me.DataContext = planes
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(goals As List(Of SP_GoalsRequiringAttentionResult))

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.requiringAttentionResults = goals
        ShowGoalsRequiringAttention
    End Sub

    Private Sub ShowGoalsRequiringAttention()
        If Not Me.requiringAttentionResults Is Nothing Then

            Dim defGoalID As Integer = requiringAttentionResults.First().definedGoalID
            Dim departmentID As Integer = requiringAttentionResults.First().DepartmentID

            Dim goals = (From g In DataLayer.GetGoalsForDepartmentByDates(departmentID,
                                                               requiringAttentionResults.First().StartDate,
                                                               requiringAttentionResults.First().EndDate).ToList()
                                                          Join reqs In Me.requiringAttentionResults
                                                          On g.GoalID Equals reqs.ID Select g).ToList()

            goals.ForEach(Sub(m) metas.Add(m))


            If goals.Count > 0 Then
                definedGoalsIds = (From i In goals Select i.DefinedGoalID).Distinct().ToList()
                definedGoalsIds.ForEach(Sub(df) AddRange((From pl In DataLayer.GetActionPlanForDefinedGoal(df) Join gl In goals
                                           On pl.GoalID Equals gl.GoalID Select pl).ToList()))
            End If
            GrdSeguimiento.ItemsSource = metas
            Me.grdActionPlan.ItemsSource = planes

        End If
    End Sub


    Private Sub CboDepartamento_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles CboDepartamento.SelectionChanged
        If Not CboDepartamento.SelectedValue Is Nothing Then
            UIElementsDataHelper.FillKPIComboboxByDepartment(Me.cboKPI, CboDepartamento.SelectedValue)
        End If
    End Sub

    Public Sub AddRange(collection As IEnumerable(Of ActionPlanViewModel))
        For Each k As ActionPlanViewModel In collection
            planes.Add(k)
        Next
    End Sub


    Private Sub ShowGoals(startDate As DateTime, endDate As DateTime)
        Dim goals = DataLayer.GetGoalsForDepartmentByDates(CboDepartamento.SelectedValue,
                                                           startDate, _
                                                           endDate,
                                                           cboKPI.SelectedValue).Where(Function(x) x.KPIName = cboKPI.Text.Trim()).ToList()
        metas.Clear()

        goals.ForEach(Sub(m) metas.Add(m))
        definedGoalsIds.Clear()
        planes.Clear()
        If goals.Count > 0 Then
            definedGoalsIds = (From i In goals Select i.DefinedGoalID).Distinct().ToList()
            definedGoalsIds.ForEach(Sub(df) AddRange(DataLayer.GetActionPlanForDefinedGoal(df)))
        End If
        Me.DataContext = metas

        GrdSeguimiento.ItemsSource = metas
        Me.grdActionPlan.ItemsSource = planes


    End Sub
    Private Sub btnBuscar_Click(sender As Object, e As RoutedEventArgs) Handles btnBuscar.Click
        If Not CboDepartamento.SelectedValue Is Nothing And Not cboKPI.SelectedValue Is Nothing And _
            Not dteFrom.SelectedDate Is Nothing And Not dteTo.SelectedDate Is Nothing Then
            metas.Clear()
            Dim minWeek As Integer = CalendarUtil.GetIso8601WeekOfYear(dteFrom.SelectedDate)
            Dim maxWeek As Integer = CalendarUtil.GetIso8601WeekOfYear(dteTo.SelectedDate)

            cboSemana.Items.Clear()
            For i As Integer = minWeek To maxWeek
                cboSemana.Items.Add(i)
            Next
            cboSemana.SelectedIndex = 0
            If rdbSemanal.IsChecked Then
                cboSemana.Visibility = Windows.Visibility.Visible
                lblSemana.Visibility = Windows.Visibility.Visible
            Else
                cboSemana.Visibility = Windows.Visibility.Hidden
                lblSemana.Visibility = Windows.Visibility.Hidden
            End If

            ShowGoals(dteFrom.SelectedDate, dteTo.SelectedDate)
   
        Else

        End If
    End Sub

    Private Sub DisplaySemana(semana As Integer)
        Dim startDate As DateTime = CalendarUtil.FirstDateOfWeekISO860(dteFrom.SelectedDate.Value.Year, semana)
        Dim endDate As DateTime = startDate.AddDays(6)
        ShowGoals(startDate, endDate)
    End Sub

    Private Sub GrdSeguimiento_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles GrdSeguimiento.SelectionChanged
        Me.GrdSeguimiento.Items.Refresh()
    End Sub

    Private Sub GrdSeguimiento_BeginningEdit(sender As Object, e As DataGridBeginningEditEventArgs) Handles GrdSeguimiento.BeginningEdit

    End Sub

    Private Sub btnAgregarPlan_Click(sender As Object, e As RoutedEventArgs) Handles btnAgregarPlan.Click
        Dim plan As New ActionPlanViewModel, responsible As MaestrodeEmpleado

        If Not Me.GrdSeguimiento.SelectedItem Is Nothing Then
            Dim selectedItem As KPIsViewModel = CType(Me.GrdSeguimiento.SelectedItem, KPIsViewModel)
            responsible = DataLayer.GetEmployeeByID(selectedItem.ResponsibleID)
            plan.PlanDate = selectedItem.GoalDate
            plan.GoalID = selectedItem.GoalID
            plan.KPIName = selectedItem.KPIName
            plan.ResponsibleID = selectedItem.ResponsibleID
            plan.Responsible = If(Not responsible Is Nothing, responsible.Nombre_empleado + " " + responsible.Apellido_empleado, "")
            plan.ComplianceDate = DateTime.Now
            plan.SelectedPriorityLevel = ActionPlanPriorityLevels.Alta
            If planes.Where(Function(x) x.GoalID = selectedItem.GoalID).FirstOrDefault Is Nothing Then
                planes.Add(plan)
            Else
                MessageBox.Show("Este indicador ya esta en el plan con esta fecha",
                            Me.Title,
                            MessageBoxButton.OK,
                            MessageBoxImage.Exclamation)
            End If
            'cboGrdPlanColumn.SelectedItemBinding = New Binding)
            Me.grdActionPlan.ItemsSource = planes

        Else
            MessageBox.Show("Por favor seleccionar un indicador",
                            Me.Title,
                            MessageBoxButton.OK,
                            MessageBoxImage.Exclamation)
        End If
    End Sub


    Private Sub btnGuardar_Click(sender As Object, e As RoutedEventArgs) Handles btnGuardar.Click
        If Me.grdActionPlan.Items.Count > 0 Then
            Dim errors = (From c In (From o In Me.grdActionPlan.ItemsSource Select Me.grdActionPlan.ItemContainerGenerator.ContainerFromItem(o)) _
                           Where Not c Is Nothing Select Validation.GetHasError(c)).FirstOrDefault()
            Dim goals As New List(Of Goal)
            Dim plans As New List(Of ActionPlan)

            If Not errors Then

                For Each planVM As ActionPlanViewModel In planes
                    Dim plan As New ActionPlan
                    plan.PriorityID = planVM.SelectedPriorityLevel
                    plan.ComplianceDate = planVM.ComplianceDate
                    plan.Progress = planVM.ProgresoNumericValue
                    plan.CorrectiveAction = planVM.CorrectiveAction
                    plan.GoalID = planVM.GoalID
                    plan.Notes = planVM.Notes
                    plan.Responsible = planVM.Responsible
                    plan.ReponsibleEmployeeID = planVM.ResponsibleID
                    plan.Situation = planVM.Situation
                    plan.ID = planVM.ID
                    plans.Add(plan)
                    DataLayer.SaveActionPlans(plans)
                    planVM.ID = plan.ID

                Next
            
            Else
                MessageBox.Show("Hay errores presentes en el plan de acción. Por favor corrija las columnas marcadas con borde rojo.",
                             Me.Title,
                             MessageBoxButton.OK,
                             MessageBoxImage.Exclamation)
            End If
        End If
        DataLayer.SaveGoalsProgress(metas.ToList())
        MessageBox.Show("Metas Actualizadas.",
                         Me.Title,
                         MessageBoxButton.OK,
                         MessageBoxImage.Information)
    End Sub

    Private Sub cboSemana_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles cboSemana.SelectionChanged
        Me.DisplaySemana(cboSemana.SelectedItem)
    End Sub
End Class
