﻿Public Class Splasher

    Private Shared _splash As Window

    Public Shared Property Splash() As Window
        Get
            Return _splash
        End Get
        Set(ByVal value As Window)
            _splash = value
        End Set
    End Property

    Public Shared Sub ShowSplash()
        If Not _splash Is Nothing Then
            _splash.Show()

        End If

    End Sub

    Public Shared Sub CloseSplash()
        If Not _splash Is Nothing Then
            _splash.Close()
            If TypeOf _splash Is System.IDisposable Then
                CType(_splash, IDisposable).Dispose()
            End If
        End If
    End Sub

End Class
