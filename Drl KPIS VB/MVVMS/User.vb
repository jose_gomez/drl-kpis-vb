﻿Public Class User
    Implements Security.Principal.IIdentity

    Private Sub New()

    End Sub
    Private Shared _UserInstance As User
    Public Property UserInstance() As User
        Get
            Return _UserInstance
        End Get
        Set(ByVal value As User)
            _UserInstance = value
        End Set
    End Property


    Private _picture As Byte()
    Public Property Picture() As Byte()
        Get
            Return _picture
        End Get
        Set(ByVal value As Byte())
            _picture = value
        End Set
    End Property


    Public Shared Function GetInstance() As User
        If _UserInstance Is Nothing Then
            Dim user As New User
            _UserInstance = user
        End If
        Return _UserInstance
    End Function

    Private _roles As List(Of String)
    Public Property Roles() As List(Of String)
        Get
            Return _roles
        End Get
        Set(ByVal value As List(Of String))
            _roles = value
        End Set
    End Property


    Private _userName As String
    Public Property UserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
        End Set
    End Property


    Private _password As String
    Public Property Password() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property

    Private _userID As Integer
    Public Property UserID() As Integer
        Get
            Return _userID
        End Get
        Set(ByVal value As Integer)
            _userID = value
        End Set
    End Property

    Private _firstName As String
    Public Property FirstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal value As String)
            _firstName = value
        End Set
    End Property


    Private _lastName As String
    Public Property LastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal value As String)
            _lastName = value
        End Set
    End Property

    Private _department As String
    Public Property Department() As String
        Get
            Return _department
        End Get
        Set(ByVal value As String)
            _department = value
        End Set
    End Property

    Private _departmentID As Integer
    Public Property DepartmentID() As Integer
        Get
            Return _departmentID
        End Get
        Set(ByVal value As Integer)
            _departmentID = value
        End Set
    End Property

    Private _isLoggedIn As Boolean
    Public Property IsLoggedIn() As Boolean
        Get
            Return _isLoggedIn
        End Get
        Set(ByVal value As Boolean)
            _isLoggedIn = value
        End Set
    End Property

    Private _role As String
    Public Property Role() As String
        Get
            Return _role
        End Get
        Set(ByVal value As String)
            _role = value
        End Set
    End Property


    Public ReadOnly Property AuthenticationType As String Implements Security.Principal.IIdentity.AuthenticationType
        Get
            Return "Custom Authentication"
        End Get
    End Property

    Public ReadOnly Property IsAuthenticated As Boolean Implements Security.Principal.IIdentity.IsAuthenticated
        Get
            Return IsLoggedIn
        End Get
    End Property

    Public ReadOnly Property Name As String Implements Security.Principal.IIdentity.Name
        Get
            Return UserName
        End Get
    End Property
End Class
