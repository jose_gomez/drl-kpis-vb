﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class UnitTest1

    <TestMethod()> Public Sub TestFirstDayOfWeekByWeekNumber()
        Dim startDate As DateTime = New DateTime(2015, 6, 5)
        Dim semana As Integer = 23
        Dim semanaCompared = Drl_KPIS_VB.CalendarUtil.FirstDateOfWeekISO860(2015, semana)
        Assert.AreEqual(New DateTime(2015, 6, 1), semanaCompared)
    End Sub

End Class