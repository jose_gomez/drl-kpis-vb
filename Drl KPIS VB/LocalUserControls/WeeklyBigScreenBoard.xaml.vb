﻿Imports Transitionals.Controls

Public Class WeeklyBigScreenBoard

    Public items As List(Of WeeklyGoalsReportItem)
    Private _semana As Integer
    Private _desde As DateTime
    Private _hasta As DateTime

    Public Property Desde As DateTime
        Get
            Return _desde
        End Get
        Set(value As DateTime)
            _desde = value
        End Set
    End Property

    Public Property Hasta As DateTime
        Get
            Return _hasta
        End Get
        Set(value As DateTime)
            _hasta = value
        End Set
    End Property

    Public Property Semana() As String
        Get
            Return _semana
        End Get
        Set(ByVal value As String)
            _semana = value
        End Set
    End Property

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Public Sub New(items As List(Of WeeklyGoalsReportItem))

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.items = items
        Me.DataContext = Me.items
    End Sub

    Private Sub grdMetricas_LoadingRow(sender As Object, e As DataGridRowEventArgs) Handles grdMetricas.LoadingRow

    End Sub

    Private Sub WeeklyBigScreenBoard_Loaded(sender As Object, e As RoutedEventArgs)
        If Not items Is Nothing Then
            If items.Count > 0 Then
                Dim deptID As Integer = items.First().DepartmentID
                Me.lblDepartamento.Content = DataLayer.GetDepartmentName(deptID)
                Me.lblSemana.Content = String.Format("Semana:{0}", _semana)
                Me.lblPeriodo.Content = String.Format("Desde:{0} Hasta: {1}", _desde.ToShortDateString(), _hasta.ToShortDateString())
            End If
            Me.grdMetricas.ItemsSource = items
            Me.grdMetricas.RowHeaderWidth = 0
        End If
    End Sub


    Private Sub grdMetricas_SelectionChanged(sender As Object, e As SelectionChangedEventArgs)
        grdMetricas.UnselectAllCells()
    End Sub

    Private Sub Label_MouseUp(sender As Object, e As MouseButtonEventArgs)
        Dim parent = CType(Me.Parent, ContentControl)

        If Not parent Is Nothing Then
            Dim slide = CType(parent.Parent, SlideshowItem)
            Dim slideshow = CType(slide.Parent, Slideshow)
            Dim contentcl = CType(slideshow.Parent, ContentControl)
            Dim grid = CType(contentcl.Parent, Grid)
            Dim window = CType(grid.Parent, Window)
            window.Close()
        End If
    End Sub

    Private Sub grdMetricas_MouseDoubleClick(sender As Object, e As MouseButtonEventArgs) Handles grdMetricas.MouseDoubleClick

    End Sub
End Class
