﻿Imports Drl.CommonControls
Class EntryPoint
    Inherits Application

    <STAThread()>
    Shared Sub Main()
        Try
            Dim screen As New SplashScreen()
            screen.SplashText = "Herramienta de Indicadores Clave DRL"
            Splasher.Splash = screen
            Splasher.ShowSplash()
            Dim login As New Login
            login.ShowDialog()
            If login.IsLogginSuccess Then
                Splasher.CloseSplash()
                Dim app = New EntryPoint()
            Else

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    

    End Sub

    Public Sub New()
   
        StartupUri = New System.Uri("MainWindow.xaml", UriKind.Relative)
        Run()
      
    End Sub

End Class
