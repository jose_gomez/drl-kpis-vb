﻿Public Class UserUtility
    Private Shared ReadOnly _user As New Lazy(Of User)(Function() User.GetInstance(), System.Threading.LazyThreadSafetyMode.ExecutionAndPublication)

    Public Shared Function GetLoggedOnUser() As User
        Return _user.Value
    End Function
End Class
