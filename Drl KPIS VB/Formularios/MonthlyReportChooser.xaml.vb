﻿Public Class MonthlyReportChooser

    Dim format As ScreenFormats
    Dim selectedDate As DateTime
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.rdbDesktop.IsChecked = True
    End Sub

    Public Sub New(screenFormat As ScreenFormats)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.format = screenFormat
        If screenFormat = ScreenFormats.StandardScreens Then
            Me.rdbDesktop.IsChecked = True
        Else
            Me.rdbLEDTV.IsChecked = True
        End If
    End Sub

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        UIElementsDataHelper.FillDepartmentsWithKPIsCombobox(cboDepartamento)
        Dim months = System.Globalization.DateTimeFormatInfo.CurrentInfo.MonthNames
        cboDepartamento.SelectedIndex = 0
        For Each s As String In months
            cboMes.Items.Add(s.ToUpper())

        Next
        cboMes.SelectedIndex = DateTime.Now.Month - 1

        FillSemanas(DateTime.Now)

       
    End Sub

    Private Sub FillSemanas(theDate As DateTime)
        Dim semanas As New List(Of Integer)
        semanas = CalendarUtil.GetWeeksInMonth(theDate)
        cboSemana.Items.Clear()
        For Each semana As Integer In semanas
            cboSemana.Items.Add(semana)
            cboSemana.SelectedIndex = 0
        Next
    End Sub

    Private Sub btnVer_Click(sender As Object, e As RoutedEventArgs) Handles btnVer.Click
        Try
            If Not cboMes.SelectedValue Is Nothing And Not cboDepartamento.SelectedValue Is Nothing Then
                rdbLEDTV_Checked(sender, e)
                Dim ind As New IndicatorBoards(Me.selectedDate, cboDepartamento.SelectedValue, Me.format) With _
                 {.WindowStyle = IIf(Me.format = ScreenFormats.BigScreens, Windows.WindowStyle.None, Windows.WindowStyle.ToolWindow) _
                 , .WindowStartupLocation = Windows.WindowStartupLocation.CenterOwner, .WindowState = Windows.WindowState.Maximized}
                ind.Show()
            Else
                MessageBox.Show("Por favor seleccionar los criterios de visualización de la pizarra",
                                Me.Title,
                                MessageBoxButton.OK,
                                MessageBoxImage.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
       

    End Sub

    Private Sub cboMes_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles cboMes.SelectionChanged
        If Not cboMes.SelectedItem Is Nothing Then
            FillSemanas(New DateTime(DateTime.Now.Year, cboMes.SelectedIndex + 1, 1))
        End If
    End Sub

    Private Sub rdbLEDTV_Checked(sender As Object, e As RoutedEventArgs) Handles rdbLEDTV.Checked
        If Not rdbLEDTV Is Nothing Then
            If rdbLEDTV.IsChecked And Not Me.cboSemana.SelectedValue Is Nothing Then
                Me.stpSemana.Visibility = Windows.Visibility.Visible
                Me.format = ScreenFormats.BigScreens
                Me.selectedDate = CalendarUtil.FirstDateOfWeekISO860(DateTime.Now.Year, Me.cboSemana.SelectedValue)
            Else
                Me.stpSemana.Visibility = Windows.Visibility.Hidden
                Me.selectedDate = New DateTime(DateTime.Now.Year, cboMes.SelectedIndex + 1, 1)
                Me.format = ScreenFormats.StandardScreens
            End If
        End If
    End Sub
End Class
