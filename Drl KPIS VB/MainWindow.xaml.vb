﻿Imports System.IO
Imports ImageUtilities.Converter
Imports System.Drawing.Imaging

Class MainWindow

    Private Sub mnuNewCategory_Click(sender As Object, e As RoutedEventArgs) Handles mnuNewCategory.Click
        Dim newcat As New AgregarCategorias
        newcat.Owner = Me
        newcat.ShowDialog()
    End Sub

    Private Sub mnuNewKPI_Click(sender As Object, e As RoutedEventArgs) Handles mnuNewKPI.Click, btnAgregarKPI.Click
        Dim newKPI As New Agregar_Nuevo_Indicador() With {.Owner = Me}
        newKPI.ShowDialog()
    End Sub

    Private Sub mnuNewGoal_Click(sender As Object, e As RoutedEventArgs) Handles mnuNewGoal.Click, btnDefinirMeta.Click
        Dim nuevaMeta As New AgregarMetas() With {.Owner = Me, .ShowInTaskbar = False}
        nuevaMeta.ShowDialog()
    End Sub

    Private Sub btnAgregarPlan_Click(sender As Object, e As RoutedEventArgs) Handles btnAgregarPlan.Click
        Dim seguirProgresos As New SeguimientoIndicadores() With _
            {.Owner = Me, .ShowInTaskbar = False, _
            .WindowStyle = Windows.WindowStyle.ToolWindow _
            , .WindowStartupLocation = Windows.WindowStartupLocation.CenterOwner}
        seguirProgresos.ShowDialog()

    End Sub

    Private Sub mnuSalir_Click(sender As Object, e As RoutedEventArgs) Handles mnuSalir.Click
        If MessageBox.Show("¿Esta seguro/a que desea salir de la aplicación?",
                           Me.Title,
                           MessageBoxButton.YesNo,
                           MessageBoxImage.Question) = vbYes Then
            Application.Current.Shutdown()
        End If
    End Sub

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            AddHandler UserBox.btnSalirClick, AddressOf mnuSalir_Click
            System.Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("es-MX")
            If (UserUtility.GetLoggedOnUser().IsLoggedIn) Then
                Dim user = UserUtility.GetLoggedOnUser()
                Dim converter As New ImageUtilities.Converter
                Dim image As New BitmapImage()
                Dim img As System.Drawing.Bitmap
                img = converter.ConvertImage(user.Picture)

                If Not img Is Nothing Then
                    Using memory As New MemoryStream
                        img.Save(memory, ImageFormat.Png)
                        memory.Position = 0
                        image = New BitmapImage()
                        image.BeginInit()
                        image.StreamSource = memory
                        image.CacheOption = BitmapCacheOption.OnLoad
                        image.EndInit()
                    End Using
                End If

                UserBox.CurrentDate = DateTime.Now.ToShortDateString()
                UserBox.Picture = image
                UserBox.UserName = user.UserName
                UserBox.Position = user.Role
                Me.Activate()
                ShowMarkers()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    
    End Sub

    Public Sub ShowMarkers()
        Dim markers = DataLayer.GetGoalsRequiringAttention()
        Dim redBackground As New LinearGradientBrush
        redBackground.GradientStops.Add(New GradientStop(Colors.White, 1))
        redBackground.GradientStops.Add(New GradientStop(Colors.Red, 0))

        Dim greenBackground As New LinearGradientBrush
        greenBackground.GradientStops.Add(New GradientStop(Colors.White, 1))
        greenBackground.GradientStops.Add(New GradientStop(Colors.Green, 0))
        lblIndicadoresSemana.Content = String.Format("Indicadores semana {0}", CalendarUtil.GetIso8601WeekOfYear(DateTime.Now))
        If markers.Count > 0 Then
            Dim requiringAttention = markers.Take(2)
            lblIndicadoresRA1.Content = requiringAttention(0).DepartmentName
            lblRA1.Content = requiringAttention(0).GoalName
            lblRA2.Content = String.Format("{0:p2}", requiringAttention(0).PercentageAchieved)
            lblRA2.Background = redBackground
            If requiringAttention.Count() > 1 Then
                lblIndicadoresRA2.Content = requiringAttention(1).DepartmentName
                lblRA3.Content = requiringAttention(1).GoalName
                lblRA4.Content = String.Format("{0:p2}", requiringAttention(1).PercentageAchieved)
                lblRA4.Background = redBackground
            End If

            Dim GoodPerformance = markers.OrderByDescending(Function(x) x.PercentageAchieved).ToList()
            If GoodPerformance.Count > 0 Then
                lblIndicadoresRA3.Content = GoodPerformance(0).DepartmentName
                lblBD1.Content = GoodPerformance(0).GoalName
                lblBD2.Content = String.Format("{0:p2}", GoodPerformance(0).PercentageAchieved)
                lblBD2.Background = greenBackground
                If GoodPerformance.Count > 1 Then
                    lblIndicadoresRA4.Content = GoodPerformance(1).DepartmentName
                    lblBD3.Content = GoodPerformance(1).GoalName
                    lblBD4.Content = String.Format("{0:p2}", GoodPerformance(1).PercentageAchieved)
                    lblBD4.Background = greenBackground
                End If
            End If
        End If
    End Sub

    Private Sub btnRA1_Click(sender As Object, e As RoutedEventArgs) Handles btnRA1.Click
        Dim markers = DataLayer.GetGoalsRequiringAttention().ToList().Take(2).ToList()
        If markers.Count > 0 Then
            Dim seg As New SeguimientoIndicadores(markers) With _
            {.Owner = Me, .ShowInTaskbar = False, _
            .WindowStyle = Windows.WindowStyle.ToolWindow _
            , .WindowStartupLocation = Windows.WindowStartupLocation.CenterOwner}
            seg.ShowDialog()
            ShowMarkers()
        End If

    End Sub

    Private Sub btnBD1_Click(sender As Object, e As RoutedEventArgs) Handles btnBD1.Click
        Dim GoodPerformance = DataLayer.GetGoalsRequiringAttention().OrderByDescending(Function(x) x.PercentageAchieved).Take(2).ToList()
        If GoodPerformance.Count > 0 Then
            Dim seg As New SeguimientoIndicadores(GoodPerformance) With _
       {.Owner = Me, .ShowInTaskbar = False, _
       .WindowStyle = Windows.WindowStyle.ToolWindow _
       , .WindowStartupLocation = Windows.WindowStartupLocation.CenterOwner}
            seg.ShowDialog()
            ShowMarkers()
        End If
    End Sub

    Private Sub btnVerKPI_Click(sender As Object, e As RoutedEventArgs) Handles btnVerKPI.Click
        Dim pzr As New MonthlyReportChooser(ScreenFormats.StandardScreens) With {.Owner = Me, .ShowInTaskbar = False, .Title = "Seleccionar periodos",
                                                     .WindowStartupLocation = Windows.WindowStartupLocation.CenterScreen, .WindowStyle = Windows.WindowStyle.ToolWindow}
        pzr.ShowDialog()
    End Sub
End Class
