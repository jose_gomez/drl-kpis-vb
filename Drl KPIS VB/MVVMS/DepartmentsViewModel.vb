﻿Public Class DepartmentsViewModel
    Private _departmentID As Integer
    Private _departmentName As String

    Public Property DeptID() As Integer
        Get
            Return _departmentID
        End Get
        Set(ByVal value As Integer)
            _departmentID = value
        End Set
    End Property

    Public Property DepartmentName() As String
        Get
            Return _departmentName
        End Get
        Set(ByVal value As String)
            _departmentName = value
        End Set
    End Property


End Class
