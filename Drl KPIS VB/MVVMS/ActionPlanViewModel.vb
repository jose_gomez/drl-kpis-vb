﻿Public Class ActionPlanViewModel

    Private _definedGoalID As Integer
    Public Property DefinedGoalID() As Integer
        Get
            Return _definedGoalID
        End Get
        Set(ByVal value As Integer)
            _definedGoalID = value
        End Set
    End Property

    Private _goalID As Integer
    Public Property GoalID() As Integer
        Get
            Return _goalID
        End Get
        Set(ByVal value As Integer)
            _goalID = value
        End Set
    End Property

    Private _selectedPriotityLevel As Integer

    Public Property SelectedPriorityLevel() As Integer
        Get
            Return _selectedPriotityLevel
        End Get
        Set(ByVal value As Integer)
            _selectedPriotityLevel = value
            Select Case value
                Case ActionPlanPriorityLevels.Alta
                    Me.PriorityLevel = "Alta"
                Case ActionPlanPriorityLevels.Media
                    Me.PriorityLevel = "Media"
                Case ActionPlanPriorityLevels.Baja
                    Me.PriorityLevel = "Baja"
            End Select
        End Set
    End Property


    Private _kpiID As Integer
    Public Property KPIID() As Integer
        Get
            Return _kpiID
        End Get
        Set(ByVal value As Integer)
            _kpiID = value
        End Set
    End Property

    Private _kpiName As String
    Public Property KPIName() As String
        Get
            Return _kpiName
        End Get
        Set(ByVal value As String)
            _kpiName = value
        End Set
    End Property



    Private _planDate As DateTime
    Public Property PlanDate() As DateTime
        Get
            Return _planDate
        End Get
        Set(ByVal value As DateTime)
            _planDate = value
        End Set
    End Property


    Private _responsible As String
    Public Property Responsible() As String
        Get
            Return _responsible
        End Get
        Set(ByVal value As String)
            _responsible = value
        End Set
    End Property


    Private _responsibleID As Integer
    Public Property ResponsibleID() As Integer
        Get
            Return _responsibleID
        End Get
        Set(ByVal value As Integer)
            _responsibleID = value
        End Set
    End Property


    Private _situation As String
    Public Property Situation() As String
        Get
            Return _situation
        End Get
        Set(ByVal value As String)
            _situation = value
        End Set
    End Property

    Private _priorityLevel As String
    Public Property PriorityLevel() As String
        Get
            Return _priorityLevel
        End Get
        Set(ByVal value As String)
            _priorityLevel = value
        End Set
    End Property

    Private _correctiveAction As String
    Public Property CorrectiveAction() As String
        Get
            Return _correctiveAction
        End Get
        Set(ByVal value As String)
            _correctiveAction = value
        End Set
    End Property

    Private _complianceDate As DateTime
    Public Property ComplianceDate() As DateTime
        Get
            Return _complianceDate
        End Get
        Set(ByVal value As DateTime)
            _complianceDate = value
        End Set
    End Property

    Private _progress As String
    Public Property Progress() As String
        Get
            Return _progress
        End Get
        Set(ByVal value As String)
            _progress = value
        End Set
    End Property

    Private _progressNumericValue As Double
    Public Property ProgresoNumericValue() As Double
        Get
            Return _progressNumericValue
        End Get
        Set(ByVal value As Double)
            _progressNumericValue = value
        End Set
    End Property

    Private _notes As String
    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal value As String)
            _notes = value
        End Set
    End Property

    Private _id As Integer
    Public Property ID() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property


End Class
