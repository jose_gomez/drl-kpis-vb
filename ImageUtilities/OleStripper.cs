﻿using Edanmo.OleStorage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ImageUtilities
{

    internal struct PackageHeader
    {
        public short Signature;
        public short HeaderSize;
        public uint ObjectType;
        public short FriendlyNameLen;
        public short ClassNameLen;
        public short FrNameOffset;
        public short ClassNameOffset;
        public int ObjectSize;
        public string FriendlyName;
        public string ClassName;
    }

    internal struct OleHeader
    {
        public uint OleVersion;
        public uint Format;
        public int ObjectTypeNameLen;
        public string ObjectTypeName;
    }

    public class OleStripper
    {
        #region Constants
        private const int FixedPackageHeaderSize = 20;
        private const int FixedOleHeaderSize = 12;
        private const int MetaFileHeaderSize = 45;
        private const int BufferSize = 1024;
        private const string ContentsEntryName = "CONTENTS";
        private const string WorkBookEntryName = "Workbook";
        private const string DocumentFriendlyName = "Document\0";
        private const string MSPhotoFriendlyName = "MSPhotoEd.3\0";
        #endregion

        private System.IO.Stream _input;
        private long _endOfHeaderPosition;
        private int _dataLength;
        private PackageHeader _packageHeader;
        private OleHeader _oleHeader;

     

        /// <summary>
        /// OleStripper constructor
        /// </summary>
        /// <param name="input">The stream to use as an input</param>
        public OleStripper(System.IO.Stream input)
        {
            _input = input;
            ReadHeader();
        }

        /// <summary>
        /// Copies the stream without headers into a memorystream and returns an instance
        /// </summary>
        /// <returns>The stream without headers</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public System.IO.Stream GetStrippedStream()
        {
            if (_input.Position != _endOfHeaderPosition && _input.CanSeek)
            {
                _input.Seek(_endOfHeaderPosition, SeekOrigin.Begin);
            }
            if (_packageHeader.ClassName.Equals(MSPhotoFriendlyName, StringComparison.OrdinalIgnoreCase))
            {
                _input.Seek(_dataLength + MetaFileHeaderSize, SeekOrigin.Current);
            }

            string tempFileName = Path.GetTempFileName();
            FileStream tempFileStream = File.OpenWrite(tempFileName);

            byte[] buffer = new byte[BufferSize];
            int loadedBytes = _input.Read(buffer, 0, BufferSize);
            while (loadedBytes > 0)
            {
                tempFileStream.Write(buffer, 0, loadedBytes);
                loadedBytes = _input.Read(buffer, 0, BufferSize);
            }
            tempFileStream.Close();

            System.IO.Stream outputStream;
            bool isCompoundFile = Storage.IsCompoundStorageFile(tempFileName);
            if (isCompoundFile)
            {
                Storage storage = new Storage(tempFileName);
                Storage.StorageElementsCollection elements = storage.Elements();
                var result = from StatStg element in elements
                             where (element.Name.Equals(ContentsEntryName, StringComparison.OrdinalIgnoreCase)
                                 || element.Name.Equals(WorkBookEntryName, StringComparison.OrdinalIgnoreCase))
                                 && element.Type == StatStg.ElementType.Stream
                             select element;
                if (result.Any())
                {
                    outputStream = storage.OpenStream(result.First().Name);
                }
                else
                {
                    storage.Close();
                    outputStream = File.OpenRead(tempFileName);
                }
            }
            else
            {
                outputStream = File.OpenRead(tempFileName);
            }
            return outputStream;
        }

        private void ReadHeader()
        {
            if (_input.Position > 0 && _input.CanSeek)
            {
                _input.Seek(0, SeekOrigin.Begin);
            }

            byte[] fixedPackageHeaderData = new byte[FixedPackageHeaderSize];
            _input.Read(fixedPackageHeaderData, 0, FixedPackageHeaderSize);

            PackageHeader packageHeader = new PackageHeader();
            packageHeader.Signature = CalcShortFromBytes(new byte[] { fixedPackageHeaderData[0], fixedPackageHeaderData[1] });
            packageHeader.HeaderSize = CalcShortFromBytes(new byte[] { fixedPackageHeaderData[2], fixedPackageHeaderData[3] });
            packageHeader.ObjectType = CalcUIntFromBytes(new byte[] { fixedPackageHeaderData[4], fixedPackageHeaderData[5], fixedPackageHeaderData[6], fixedPackageHeaderData[7] });
            packageHeader.FriendlyNameLen = CalcShortFromBytes(new byte[] { fixedPackageHeaderData[8], fixedPackageHeaderData[9] });
            packageHeader.ClassNameLen = CalcShortFromBytes(new byte[] { fixedPackageHeaderData[10], fixedPackageHeaderData[11] });
            packageHeader.FrNameOffset = CalcShortFromBytes(new byte[] { fixedPackageHeaderData[12], fixedPackageHeaderData[13] });
            packageHeader.ClassNameOffset = CalcShortFromBytes(new byte[] { fixedPackageHeaderData[14], fixedPackageHeaderData[15] });
            packageHeader.ObjectSize = CalcIntFromBytes(new byte[] { fixedPackageHeaderData[16], fixedPackageHeaderData[17], fixedPackageHeaderData[18], fixedPackageHeaderData[19] });

            byte[] friendlyNameData = new byte[packageHeader.FriendlyNameLen];
            _input.Read(friendlyNameData, 0, packageHeader.FriendlyNameLen);
            packageHeader.FriendlyName = Encoding.UTF8.GetString(friendlyNameData);

            byte[] classNameData = new byte[packageHeader.ClassNameLen];
            _input.Read(classNameData, 0, packageHeader.ClassNameLen);
            packageHeader.ClassName = Encoding.UTF8.GetString(classNameData);

            _packageHeader = packageHeader;

            byte[] fixedOleHeaderData = new byte[FixedOleHeaderSize];
            _input.Read(fixedOleHeaderData, 0, FixedOleHeaderSize);

            OleHeader oleHeader = new OleHeader();
            oleHeader.OleVersion = CalcUIntFromBytes(new byte[] { fixedOleHeaderData[0], fixedOleHeaderData[1], fixedOleHeaderData[2], fixedOleHeaderData[3] });
            oleHeader.Format = CalcUIntFromBytes(new byte[] { fixedOleHeaderData[4], fixedOleHeaderData[5], fixedOleHeaderData[6], fixedOleHeaderData[7] });
            oleHeader.ObjectTypeNameLen = CalcIntFromBytes(new byte[] { fixedOleHeaderData[8], fixedOleHeaderData[9], fixedOleHeaderData[10], fixedOleHeaderData[11] });

            byte[] objectTypeNameData = new byte[oleHeader.ObjectTypeNameLen];
            _input.Read(objectTypeNameData, 0, oleHeader.ObjectTypeNameLen);
            oleHeader.ObjectTypeName = Encoding.UTF8.GetString(objectTypeNameData);

            _oleHeader = oleHeader;

            for (int index = 0; index < 8; index++)
            {
                _input.ReadByte();
            }

            byte[] lengthData = new byte[4];
            _input.Read(lengthData, 0, 4);
            _dataLength = BitConverter.ToInt32(lengthData, 0);

            _endOfHeaderPosition = _input.Position;
        }

        #region Calculation methods

        private static short CalcShortFromBytes(byte[] data)
        {
            short result = (short)((data[1] * 128) + data[0]);
            return result;
        }

        private static uint CalcUIntFromBytes(byte[] data)
        {
            uint result = (uint)((data[3] * 16777216) + (data[2] * 32768) + (data[1] * 256) + data[0]);
            return result;
        }

        private static int CalcIntFromBytes(byte[] data)
        {
            int result = (int)((data[3] * 8388608) + (data[2] * 32768) + (data[1] * 256) + data[0]);
            return result;
        }
        #endregion
    }
}
