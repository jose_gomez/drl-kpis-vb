﻿Public Class SplashScreen
    Private _splashText As String
    Public Property SplashText() As String
        Get
            Return _splashText
        End Get
        Set(ByVal value As String)
            _splashText = value
            Me.lblSplashText.Content = _splashText
        End Set
    End Property


End Class
