USE [KeyPerformanceIndicators]
GO
/****** Object:  Table [dbo].[Departments]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Departments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentName] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Departments] ON
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (1, N'PULIDORES')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (2, N'PRODUCCION-TK')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (3, N'ENSAMBLE-REG')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (4, N'N. PRODUCTOS')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (5, N'CALIDAD')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (6, N'MURANO')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (7, N'HORNO-II')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (8, N'PERSONAL')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (9, N'STONE SETTING')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (10, N'BANGLES')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (11, N'HORNO-I')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (12, N'REMACHE')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (13, N'EMBARQUE')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (14, N'LABORATORIO')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (15, N'INGENIERIA')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (16, N'ENSAMBLE')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (17, N'ROPE CHAIN')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (18, N'CELDA')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (19, N'INVENTARIO')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (20, N'MURANO/SOFTGOLD')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (21, N'PLANIFICACION')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (22, N'SARDELLI')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (23, N'ADMINISTRACION')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (24, N'SEGURIDAD')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (25, N'INVERNESS')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (26, N'PULIDORES-TK')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (27, N'LAVADO-TOMBOLA')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (28, N'CONTROL')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (29, N'CORTE')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (30, N'SUPERVISORES')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (31, N'RHODIUM')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (32, N'GRABADORES')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (33, N'JOYERIA')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (34, N'HIGIENE Y SEGURIDAD OCUPACIONAL')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (35, N'ENSAMBLE-ECL')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (36, N'CONTABILIDAD')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (37, N'AQL')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (38, N'ALMACEN')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (39, N'COORPORATIVO')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (40, N'MANIPULADO-TK')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (41, N'GESTION HUMANA')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (42, N'PRODUCCION')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (43, N'HORNO')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (44, N'COMPRAS')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (45, N'MANIPULADO')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (46, N'EMPAQUE')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (47, N'RODHIUM')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (48, N'COMPRAS Y PLANIFICACION')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (49, N'RECEIVING')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (50, N'GRADUAL CHAIN')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (51, N'CASTING')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (52, N'MANTENIMIENTO')
INSERT [dbo].[Departments] ([ID], [DepartmentName]) VALUES (53, N'SEGURIDAD Y RRHH')
SET IDENTITY_INSERT [dbo].[Departments] OFF
/****** Object:  Table [dbo].[Categories]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categories](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Categories] ON
INSERT [dbo].[Categories] ([ID], [CategoryName]) VALUES (2, N'SEGURIDAD Y RRHH')
INSERT [dbo].[Categories] ([ID], [CategoryName]) VALUES (3, N'CALIDAD')
INSERT [dbo].[Categories] ([ID], [CategoryName]) VALUES (4, N'DESEMPEÑO')
INSERT [dbo].[Categories] ([ID], [CategoryName]) VALUES (5, N'SERVICIO')
INSERT [dbo].[Categories] ([ID], [CategoryName]) VALUES (6, N'SISTEMAS')
INSERT [dbo].[Categories] ([ID], [CategoryName]) VALUES (7, N'CONTROL DE METAL')
INSERT [dbo].[Categories] ([ID], [CategoryName]) VALUES (8, N'Producción')
SET IDENTITY_INSERT [dbo].[Categories] OFF
/****** Object:  Table [dbo].[ActionPlanPriorities]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionPlanPriorities](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ActionPlanPriorities] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ActionPlanPriorities] ON
INSERT [dbo].[ActionPlanPriorities] ([ID], [Description]) VALUES (1, N'Alta')
INSERT [dbo].[ActionPlanPriorities] ([ID], [Description]) VALUES (2, N'Media')
INSERT [dbo].[ActionPlanPriorities] ([ID], [Description]) VALUES (3, N'Baja')
SET IDENTITY_INSERT [dbo].[ActionPlanPriorities] OFF
/****** Object:  Table [dbo].[PeriodFrequencies]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PeriodFrequencies](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PeriodFrequencies] ON
INSERT [dbo].[PeriodFrequencies] ([ID], [Description]) VALUES (1, N'Diario')
INSERT [dbo].[PeriodFrequencies] ([ID], [Description]) VALUES (2, N'Semanal')
SET IDENTITY_INSERT [dbo].[PeriodFrequencies] OFF
/****** Object:  Table [dbo].[MatchingCriterias]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MatchingCriterias](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[MatchingCriterias] ON
INSERT [dbo].[MatchingCriterias] ([ID], [Description]) VALUES (1, N'Maxima')
INSERT [dbo].[MatchingCriterias] ([ID], [Description]) VALUES (2, N'Minima')
SET IDENTITY_INSERT [dbo].[MatchingCriterias] OFF
/****** Object:  Table [dbo].[CategoriesDepartments]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoriesDepartments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[DepartmentID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPIS]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KPIS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[KPIName] [varchar](200) NULL,
	[CategoryID] [int] NULL,
	[DepartmentID] [int] NULL,
	[Username] [varchar](200) NULL,
	[OSUser] [varchar](200) NULL,
	[WorkStation] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[KPIS] ON
INSERT [dbo].[KPIS] ([ID], [KPIName], [CategoryID], [DepartmentID], [Username], [OSUser], [WorkStation]) VALUES (1, N'Accidentabilidad', 2, 21, NULL, NULL, NULL)
INSERT [dbo].[KPIS] ([ID], [KPIName], [CategoryID], [DepartmentID], [Username], [OSUser], [WorkStation]) VALUES (2, N'Auditoria ', 2, 23, NULL, NULL, NULL)
INSERT [dbo].[KPIS] ([ID], [KPIName], [CategoryID], [DepartmentID], [Username], [OSUser], [WorkStation]) VALUES (3, N'Accidentabilidad', 2, 23, NULL, NULL, NULL)
INSERT [dbo].[KPIS] ([ID], [KPIName], [CategoryID], [DepartmentID], [Username], [OSUser], [WorkStation]) VALUES (4, N'KPI Prueba', 4, 23, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[KPIS] OFF
/****** Object:  Table [dbo].[Employees]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employees](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](200) NULL,
	[LastName] [varchar](200) NULL,
	[DepartmentID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Employees] ON
INSERT [dbo].[Employees] ([ID], [FirstName], [LastName], [DepartmentID]) VALUES (1, N'Jose', N'Gomez', 1)
SET IDENTITY_INSERT [dbo].[Employees] OFF
/****** Object:  Table [dbo].[DefinedGoals]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DefinedGoals](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NULL,
	[StartDate] [datetime] NULL,
	[GoalName] [varchar](200) NULL,
	[EndDate] [datetime] NULL,
	[KPIID] [int] NULL,
	[CreatedBy] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DefinedGoals] ON
INSERT [dbo].[DefinedGoals] ([ID], [DepartmentID], [StartDate], [GoalName], [EndDate], [KPIID], [CreatedBy]) VALUES (5, 23, CAST(0x0000A4DB00000000 AS DateTime), NULL, CAST(0x0000A4E000000000 AS DateTime), 1, NULL)
INSERT [dbo].[DefinedGoals] ([ID], [DepartmentID], [StartDate], [GoalName], [EndDate], [KPIID], [CreatedBy]) VALUES (6, 23, CAST(0x0000A4DF00000000 AS DateTime), NULL, CAST(0x0000A4E000000000 AS DateTime), 1, NULL)
INSERT [dbo].[DefinedGoals] ([ID], [DepartmentID], [StartDate], [GoalName], [EndDate], [KPIID], [CreatedBy]) VALUES (7, 23, CAST(0x0000A4E000000000 AS DateTime), NULL, CAST(0x0000A4E000000000 AS DateTime), 1, NULL)
INSERT [dbo].[DefinedGoals] ([ID], [DepartmentID], [StartDate], [GoalName], [EndDate], [KPIID], [CreatedBy]) VALUES (8, 23, CAST(0x0000A4E100000000 AS DateTime), NULL, CAST(0x0000A4E100000000 AS DateTime), 1, NULL)
INSERT [dbo].[DefinedGoals] ([ID], [DepartmentID], [StartDate], [GoalName], [EndDate], [KPIID], [CreatedBy]) VALUES (9, 23, CAST(0x0000A4E200000000 AS DateTime), NULL, CAST(0x0000A4E200000000 AS DateTime), 1, NULL)
INSERT [dbo].[DefinedGoals] ([ID], [DepartmentID], [StartDate], [GoalName], [EndDate], [KPIID], [CreatedBy]) VALUES (10, 23, CAST(0x0000A4C800000000 AS DateTime), NULL, CAST(0x0000A4E600000000 AS DateTime), 1, NULL)
INSERT [dbo].[DefinedGoals] ([ID], [DepartmentID], [StartDate], [GoalName], [EndDate], [KPIID], [CreatedBy]) VALUES (11, 23, CAST(0x0000A4DB00000000 AS DateTime), NULL, CAST(0x0000A4E000000000 AS DateTime), 1, NULL)
INSERT [dbo].[DefinedGoals] ([ID], [DepartmentID], [StartDate], [GoalName], [EndDate], [KPIID], [CreatedBy]) VALUES (12, 23, CAST(0x0000A4DB00000000 AS DateTime), NULL, CAST(0x0000A4E000000000 AS DateTime), 3, NULL)
SET IDENTITY_INSERT [dbo].[DefinedGoals] OFF
/****** Object:  Table [dbo].[Goals]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Goals](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GoalName] [varchar](200) NULL,
	[KPIID] [int] NULL,
	[EmployeeID] [int] NULL,
	[departmentID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[FrequencyID] [int] NULL,
	[ExpectedGoalValue] [decimal](18, 0) NULL,
	[MatchingValueCriteriaID] [int] NULL,
	[AchievedValue] [decimal](18, 0) NULL,
	[WeekNumber] [int] NULL,
	[DefinedGoalID] [int] NULL,
	[DefinedBy] [int] NULL,
	[SavedBy] [int] NULL,
	[SavedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Goals] ON
INSERT [dbo].[Goals] ([ID], [GoalName], [KPIID], [EmployeeID], [departmentID], [StartDate], [EndDate], [FrequencyID], [ExpectedGoalValue], [MatchingValueCriteriaID], [AchievedValue], [WeekNumber], [DefinedGoalID], [DefinedBy], [SavedBy], [SavedOn]) VALUES (129, N'Accidentabilidad', 3, 1, 23, CAST(0x0000A4DB00000000 AS DateTime), CAST(0x0000A4DB00000000 AS DateTime), 1, CAST(30 AS Decimal(18, 0)), 1, CAST(28 AS Decimal(18, 0)), 30, 12, NULL, 0, CAST(0x0000A4E000C310EF AS DateTime))
INSERT [dbo].[Goals] ([ID], [GoalName], [KPIID], [EmployeeID], [departmentID], [StartDate], [EndDate], [FrequencyID], [ExpectedGoalValue], [MatchingValueCriteriaID], [AchievedValue], [WeekNumber], [DefinedGoalID], [DefinedBy], [SavedBy], [SavedOn]) VALUES (130, N'Accidentabilidad', 3, 1, 23, CAST(0x0000A4DC00000000 AS DateTime), CAST(0x0000A4DC00000000 AS DateTime), 1, CAST(30 AS Decimal(18, 0)), 1, CAST(35 AS Decimal(18, 0)), 30, 12, NULL, 0, CAST(0x0000A4E000C31179 AS DateTime))
INSERT [dbo].[Goals] ([ID], [GoalName], [KPIID], [EmployeeID], [departmentID], [StartDate], [EndDate], [FrequencyID], [ExpectedGoalValue], [MatchingValueCriteriaID], [AchievedValue], [WeekNumber], [DefinedGoalID], [DefinedBy], [SavedBy], [SavedOn]) VALUES (131, N'Accidentabilidad', 3, 1, 23, CAST(0x0000A4DD00000000 AS DateTime), CAST(0x0000A4DD00000000 AS DateTime), 1, CAST(30 AS Decimal(18, 0)), 1, CAST(25 AS Decimal(18, 0)), 30, 12, NULL, 0, CAST(0x0000A4E000C3118A AS DateTime))
INSERT [dbo].[Goals] ([ID], [GoalName], [KPIID], [EmployeeID], [departmentID], [StartDate], [EndDate], [FrequencyID], [ExpectedGoalValue], [MatchingValueCriteriaID], [AchievedValue], [WeekNumber], [DefinedGoalID], [DefinedBy], [SavedBy], [SavedOn]) VALUES (132, N'Accidentabilidad', 3, 1, 23, CAST(0x0000A4DE00000000 AS DateTime), CAST(0x0000A4DE00000000 AS DateTime), 1, CAST(30 AS Decimal(18, 0)), 1, CAST(31 AS Decimal(18, 0)), 30, 12, NULL, 0, CAST(0x0000A4E000C311A1 AS DateTime))
INSERT [dbo].[Goals] ([ID], [GoalName], [KPIID], [EmployeeID], [departmentID], [StartDate], [EndDate], [FrequencyID], [ExpectedGoalValue], [MatchingValueCriteriaID], [AchievedValue], [WeekNumber], [DefinedGoalID], [DefinedBy], [SavedBy], [SavedOn]) VALUES (133, N'Accidentabilidad', 3, 1, 23, CAST(0x0000A4DF00000000 AS DateTime), CAST(0x0000A4DF00000000 AS DateTime), 1, CAST(30 AS Decimal(18, 0)), 1, CAST(33 AS Decimal(18, 0)), 30, 12, NULL, 0, CAST(0x0000A4E000C311A6 AS DateTime))
INSERT [dbo].[Goals] ([ID], [GoalName], [KPIID], [EmployeeID], [departmentID], [StartDate], [EndDate], [FrequencyID], [ExpectedGoalValue], [MatchingValueCriteriaID], [AchievedValue], [WeekNumber], [DefinedGoalID], [DefinedBy], [SavedBy], [SavedOn]) VALUES (134, N'Accidentabilidad', 3, 1, 23, CAST(0x0000A4E000000000 AS DateTime), CAST(0x0000A4E000000000 AS DateTime), 1, CAST(30 AS Decimal(18, 0)), 1, CAST(25 AS Decimal(18, 0)), 30, 12, NULL, 0, CAST(0x0000A4E000C311AA AS DateTime))
SET IDENTITY_INSERT [dbo].[Goals] OFF
/****** Object:  Table [dbo].[ActionPlans]    Script Date: 07/25/2015 11:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionPlans](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Responsible] [varchar](200) NULL,
	[GoalID] [int] NULL,
	[ComplianceDate] [datetime] NULL,
	[Notes] [varchar](1000) NULL,
	[Situation] [varchar](200) NULL,
	[PriorityID] [int] NULL,
	[CorrectiveAction] [varchar](300) NULL,
	[Progress] [decimal](18, 0) NULL,
 CONSTRAINT [PK_ActionPlans] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ActionPlans] ON
INSERT [dbo].[ActionPlans] ([ID], [Responsible], [GoalID], [ComplianceDate], [Notes], [Situation], [PriorityID], [CorrectiveAction], [Progress]) VALUES (9, N'Juan Andujar', 129, CAST(0x0000A4E100000000 AS DateTime), N'Se le paso arreglos a equipo de freddy para que lo ensamblen', N'Se corrigio estampa', 2, N'Enviar estampa a ensambole', CAST(50 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[ActionPlans] OFF
/****** Object:  ForeignKey [FK_ActionPlans_ActionPlanPriorities]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[ActionPlans]  WITH CHECK ADD  CONSTRAINT [FK_ActionPlans_ActionPlanPriorities] FOREIGN KEY([PriorityID])
REFERENCES [dbo].[ActionPlanPriorities] ([ID])
GO
ALTER TABLE [dbo].[ActionPlans] CHECK CONSTRAINT [FK_ActionPlans_ActionPlanPriorities]
GO
/****** Object:  ForeignKey [FK_ActionPlans_Goals]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[ActionPlans]  WITH CHECK ADD  CONSTRAINT [FK_ActionPlans_Goals] FOREIGN KEY([GoalID])
REFERENCES [dbo].[Goals] ([ID])
GO
ALTER TABLE [dbo].[ActionPlans] CHECK CONSTRAINT [FK_ActionPlans_Goals]
GO
/****** Object:  ForeignKey [FK__Categorie__Categ__0DAF0CB0]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[CategoriesDepartments]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([ID])
GO
/****** Object:  ForeignKey [FK__Categorie__Depar__0EA330E9]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[CategoriesDepartments]  WITH CHECK ADD FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[Departments] ([ID])
GO
/****** Object:  ForeignKey [FK__GoalsTrac__Depar__267ABA7A]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[DefinedGoals]  WITH CHECK ADD FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[Departments] ([ID])
GO
/****** Object:  ForeignKey [FK_DefinedGoals_KPIS]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[DefinedGoals]  WITH CHECK ADD  CONSTRAINT [FK_DefinedGoals_KPIS] FOREIGN KEY([KPIID])
REFERENCES [dbo].[KPIS] ([ID])
GO
ALTER TABLE [dbo].[DefinedGoals] CHECK CONSTRAINT [FK_DefinedGoals_KPIS]
GO
/****** Object:  ForeignKey [FK__Employees__Depar__1367E606]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[Departments] ([ID])
GO
/****** Object:  ForeignKey [FK__Goals__departmen__1A14E395]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD FOREIGN KEY([departmentID])
REFERENCES [dbo].[Departments] ([ID])
GO
/****** Object:  ForeignKey [FK__Goals__EmployeeI__1920BF5C]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([ID])
GO
/****** Object:  ForeignKey [FK__Goals__KPIID__182C9B23]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD FOREIGN KEY([KPIID])
REFERENCES [dbo].[KPIS] ([ID])
GO
/****** Object:  ForeignKey [FK_Goals_DefinedGoals]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD  CONSTRAINT [FK_Goals_DefinedGoals] FOREIGN KEY([DefinedGoalID])
REFERENCES [dbo].[DefinedGoals] ([ID])
GO
ALTER TABLE [dbo].[Goals] CHECK CONSTRAINT [FK_Goals_DefinedGoals]
GO
/****** Object:  ForeignKey [FK_Goals_MatchingCriterias]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD  CONSTRAINT [FK_Goals_MatchingCriterias] FOREIGN KEY([MatchingValueCriteriaID])
REFERENCES [dbo].[MatchingCriterias] ([ID])
GO
ALTER TABLE [dbo].[Goals] CHECK CONSTRAINT [FK_Goals_MatchingCriterias]
GO
/****** Object:  ForeignKey [FK_Goals_PeriodFrequencies]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD  CONSTRAINT [FK_Goals_PeriodFrequencies] FOREIGN KEY([FrequencyID])
REFERENCES [dbo].[PeriodFrequencies] ([ID])
GO
ALTER TABLE [dbo].[Goals] CHECK CONSTRAINT [FK_Goals_PeriodFrequencies]
GO
/****** Object:  ForeignKey [FK__KPIS__CategoryID__08EA5793]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[KPIS]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([ID])
GO
/****** Object:  ForeignKey [FK_KPIS_Departments]    Script Date: 07/25/2015 11:52:52 ******/
ALTER TABLE [dbo].[KPIS]  WITH CHECK ADD  CONSTRAINT [FK_KPIS_Departments] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[Departments] ([ID])
GO
ALTER TABLE [dbo].[KPIS] CHECK CONSTRAINT [FK_KPIS_Departments]
GO
