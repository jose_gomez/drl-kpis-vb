﻿Public Class MessageListener
    Inherits DependencyObject
    Private Shared _instance As MessageListener

    Private Sub New()

    End Sub

    Public Shared ReadOnly Property Instance() As MessageListener
        Get
            If _instance Is Nothing Then
                _instance = New MessageListener
            End If
            Return _instance
        End Get

    End Property

    Public Sub ReceiveMessage(message As String)

    End Sub

    Private _message As String

    Public Property Message() As String
        Get
            Return _message
        End Get
        Set(ByVal value As String)
            SetValue(MessageProperty, value)
        End Set
    End Property

    Public Shared ReadOnly MessageProperty As DependencyProperty _
        = DependencyProperty.Register("Message", GetType(String), GetType(MessageListener), New UIPropertyMetadata(Nothing))


End Class
