﻿Public Class WeeklyGoalsReportItem
    Private _goalName As String
    Private _matchingValueCriteriaID As Integer
    Private _goal As Double
    Private _monday As Double
    Private _tuesday As Double
    Private _wednesday As Double
    Private _thursday As Double
    Private _friday As Double
    Private _saturday As Double
    Private _deptID As Integer

    Private _DefinedGoalID As Integer
    Public Property DefinedGoalID() As Integer
        Get
            Return _DefinedGoalID
        End Get
        Set(ByVal value As Integer)
            _DefinedGoalID = value
        End Set
    End Property


    Public Property DepartmentID
        Get
            Return _deptID
        End Get
        Set(value)
            _deptID = value
        End Set
    End Property

    Public Property GoalName() As String
        Get
            Return _goalName
        End Get
        Set(ByVal value As String)
            _goalName = value
        End Set
    End Property


    Public Property MatchingValueCriteriaID() As Integer
        Get
            Return _matchingValueCriteriaID
        End Get
        Set(ByVal value As Integer)
            _matchingValueCriteriaID = value
        End Set
    End Property


    Public Property Goal() As Double
        Get
            Return _goal
        End Get
        Set(ByVal value As Double)
            _goal = value
        End Set
    End Property


    Public Property Monday() As Double
        Get
            Return _monday
        End Get
        Set(ByVal value As Double)
            _monday = value
        End Set
    End Property


    Public Property Tuesday() As Double
        Get
            Return _tuesday
        End Get
        Set(ByVal value As Double)
            _tuesday = value
        End Set
    End Property


    Public Property Wednesday() As Double
        Get
            Return _wednesday
        End Get
        Set(ByVal value As Double)
            _wednesday = value
        End Set
    End Property


    Public Property Thursday() As Double
        Get
            Return _thursday
        End Get
        Set(ByVal value As Double)
            _thursday = value
        End Set
    End Property



    Public Property Friday() As Double
        Get
            Return _friday
        End Get
        Set(ByVal value As Double)
            _friday = value
        End Set
    End Property


    Public Property Saturday() As Double
        Get
            Return _saturday
        End Get
        Set(ByVal value As Double)
            _saturday = value
        End Set
    End Property


    Private _sunday As Double
    Public Property Sunday() As Double
        Get
            Return _sunday
        End Get
        Set(ByVal value As Double)
            _sunday = value
        End Set
    End Property


End Class
