﻿Public Class RoleToEnableAbilityConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        If UserUtility.GetLoggedOnUser().IsLoggedIn Then
            Return CType(value, List(Of String)).Any(Function(x) x = UserUtility.GetLoggedOnUser().Role)
        Else
            Return False
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class
