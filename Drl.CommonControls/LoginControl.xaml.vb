﻿Public Class LoginControl
    Public Event iniciarClick As RoutedEventHandler
    Public Event cancelarClick As RoutedEventHandler

    Private Sub btnIniciarSesion_Click(sender As Object, e As RoutedEventArgs) Handles btnIniciarSesion.Click
            RaiseEvent iniciarClick(sender, e)
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As RoutedEventArgs) Handles btnCancelar.Click
        RaiseEvent cancelarClick(sender, e)
    End Sub

    Private _userName As String
    Public Property UserName() As String
        Get
            _userName = txtUserName.Text
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
            txtUserName.Text = _userName
        End Set
    End Property

    Private _password As String
    Public Property Password() As String
        Get
            _password = txtPassword.Password
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
            txtPassword.Password = _password
        End Set
    End Property

    Private Sub txtPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPassword.KeyDown

        If e.Key = Key.Enter Then
            RaiseEvent iniciarClick(sender, e)
        End If
    End Sub
End Class
