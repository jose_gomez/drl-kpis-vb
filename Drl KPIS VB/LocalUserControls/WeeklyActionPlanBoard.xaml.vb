﻿Imports Transitionals.Controls

Public Class WeeklyActionPlanBoard

    Private _actionPlans As New List(Of ActionPlanViewModel)


    Public Property ActionPlans() As List(Of ActionPlanViewModel)
        Get
            Return _actionPlans
        End Get
        Set(ByVal value As List(Of ActionPlanViewModel))
            _actionPlans = value
        End Set
    End Property


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub Refresh()
        Me.grdActionPlan.ItemsSource = _actionPlans
        Me.grdActionPlan.Items.Refresh()
    End Sub

    Public Sub New(Plans As List(Of ActionPlanViewModel))
        InitializeComponent()
        Me._actionPlans = Plans
    End Sub

    Private Sub Label_MouseUp(sender As Object, e As MouseButtonEventArgs)

    End Sub

    Private Sub grdActionPlan_MouseDoubleClick(sender As Object, e As MouseButtonEventArgs) Handles grdActionPlan.MouseDoubleClick
        Dim parent = CType(Me.Parent, ContentControl)

        If Not parent Is Nothing Then
            Dim grid = CType(parent.Parent, Grid)
            Dim window = CType(grid.Parent, Window)
            window.Close()
        End If
    End Sub
End Class
