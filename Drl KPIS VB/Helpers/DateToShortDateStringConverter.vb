﻿Public Class DateToShortDateStringConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        If Not value Is Nothing Then
            Return DateTime.Parse(value).ToShortDateString()
        Else
            Return DateTime.Now.ToShortDateString()
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Return DateTime.Parse(value)
    End Function
End Class
