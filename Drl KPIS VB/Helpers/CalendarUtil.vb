﻿Imports System.Globalization
Imports System.Runtime.CompilerServices

Public Class CalendarUtil
    ''' <summary>
    ''' Gets the year's week number for the date provided .
    ''' </summary>
    ''' <param name="time">The date to calculate the week number from</param>
    ''' <returns>Week Number as integer</returns>
    ''' <remarks>Jose Gomez 07/22/2015</remarks>
    Public Shared Function GetIso8601WeekOfYear(time As DateTime) As Integer

        Dim day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time)
        If day >= DayOfWeek.Monday And day <= DayOfWeek.Wednesday Then
            time = time.AddDays(3)
        End If

        Return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday)
    End Function

    Public Shared Function FirstDayOfMonth(theDate As DateTime) As DateTime
        Return New DateTime(theDate.Year, theDate.Month, 1)
    End Function

    ''' <summary>
    ''' Gets the first day for a given year's week number
    ''' </summary>
    ''' <param name="year">The year you want to get the first day from for the week number provided</param>
    ''' <param name="weekNumber">The week number provided</param>
    ''' <returns>The first day for the provided week number in the year passed as a parameter</returns>
    ''' <remarks>Jose Gomez 07/22/2015</remarks>
    Public Shared Function FirstDateOfWeekISO860(year As Integer, weekNumber As Integer) As DateTime
        Dim jan1 As DateTime = New DateTime(year, 1, 1)
        Dim daysOffset As Integer = DayOfWeek.Thursday - jan1.DayOfWeek
        Dim firstThursday As DateTime = jan1.AddDays(daysOffset)
        Dim cal = CultureInfo.CurrentCulture.Calendar
        Dim firstWeek As Integer = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday)
        Dim weekNum = weekNumber
        If firstWeek <= 1 Then
            weekNum -= 1
        End If
        Dim result = firstThursday.AddDays(weekNum * 7)
        Return result.AddDays(-3)
    End Function


    Public Shared Function GetWeeksInMonth(theDate As DateTime) As List(Of Integer)

        Dim firstDay As DateTime = New DateTime(theDate.Year, theDate.Month, 1)
        Dim lastDay As DateTime = New DateTime(theDate.Year, theDate.Month, DateTime.DaysInMonth(theDate.Year, theDate.Month))

        Dim firstWeek As Integer = GetIso8601WeekOfYear(firstDay)
        Dim lastWeek As Integer = GetIso8601WeekOfYear(lastDay)
        Dim weeks As New List(Of Integer)
        For i As Integer = firstWeek To lastWeek
            weeks.Add(i)
        Next
        Return weeks
    End Function

 


End Class
