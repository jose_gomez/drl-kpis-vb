﻿Public Class KPIsViewModel
    Private _id As Integer
    Private _departmentID As String
    Private _kpiName As String
    Private _categoryID As Integer
    Private _departmentName As String
    Private _desiredGoal As Double
    Private _isClosed As Boolean
    Private _definedGoalID As Integer
    Private _achievedValue As String
    Private _percentageAchieved As Double
    Private _goalID As Integer
    ReadOnly _background As LinearGradientBrush
    Private _categoryName As String
    Private _goalDirection As GoalDirections
    Private _goalDate As DateTime
    Private _responsible As String
    Private _responsibleID As Integer
    Private _fullName As String

    Private _previousPeriod As String
    Public Property PreviousPeriod() As String
        Get
            Return _previousPeriod
        End Get
        Set(ByVal value As String)
            _previousPeriod = value
        End Set
    End Property



    Public Property FullName As String
        Get
            Return _fullName
        End Get
        Set(value As String)
            _fullName = value
        End Set
    End Property

    Public Property ResponsibleID As Integer
        Get
            Return _responsibleID
        End Get
        Set(value As Integer)
            _responsibleID = value
        End Set

    End Property


    Private _weekNumber As Integer
    Public Property WeekNumber() As Integer
        Get
            Return _weekNumber
        End Get
        Set(ByVal value As Integer)
            _weekNumber = value
        End Set
    End Property


    Public Property Responsible() As String
        Get
            Return _responsible
        End Get
        Set(ByVal value As String)
            _responsible = value
        End Set
    End Property


    Public Property DesiredGoal As Double
        Get
            Return _desiredGoal
        End Get
        Set(value As Double)
            _desiredGoal = value
        End Set
    End Property


    Public Property IsClosed() As Boolean
        Get
            Return _isClosed
        End Get
        Set(ByVal value As Boolean)
            _isClosed = value
        End Set
    End Property


    Public Property DefinedGoalID() As Integer
        Get
            Return _definedGoalID
        End Get
        Set(ByVal value As Integer)
            _definedGoalID = value
        End Set
    End Property


    Public Property AchievedValue() As String
        Get
            Return _achievedValue
        End Get
        Set(ByVal value As String)
            _achievedValue = value
        End Set
    End Property



    Public Property PercentageAchieved() As Double
        Get
            If Me.AchievedValue > 0 And Me.DesiredGoal > 0 Then
                _percentageAchieved = (Me.AchievedValue / Me.DesiredGoal) * 100
            End If
            Return _percentageAchieved
        End Get
        Set(ByVal value As Double)
            If Me.AchievedValue > 0 And Me.DesiredGoal > 0 Then
                _percentageAchieved = (Me.AchievedValue / Me.DesiredGoal) * 100
            End If
        End Set
    End Property


    Public Property GoalID() As Integer
        Get
            Return _goalID
        End Get
        Set(ByVal value As Integer)
            _goalID = value
        End Set
    End Property


    Public ReadOnly Property Background() As LinearGradientBrush
        Get
            Dim lgb As New LinearGradientBrush
            lgb.GradientStops.Add(New GradientStop(Colors.White, 1))
            If Me.PercentageAchieved <> 0 Then
                If Me.GoalDirection = GoalDirections.Top Then
                    If Me.PercentageAchieved >= 100 Then
                        lgb.GradientStops.Add(New GradientStop(Colors.Green, 0))
                    Else
                        lgb.GradientStops.Add(New GradientStop(Colors.Red, 0))
                    End If
                Else
                    If Me.PercentageAchieved <= 100 Then
                        lgb.GradientStops.Add(New GradientStop(Colors.Green, 0))
                    Else
                        lgb.GradientStops.Add(New GradientStop(Colors.Red, 0))
                    End If
                End If
            Else

            End If
            Return lgb
        End Get

    End Property


    Public Property ID() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property


    Public Property KPIName() As String
        Get
            Return _kpiName
        End Get
        Set(ByVal value As String)
            _kpiName = value
        End Set
    End Property


    Public Property DepartmentID() As String
        Get
            Return _departmentID
        End Get
        Set(ByVal value As String)
            _departmentID = value
        End Set
    End Property


    Public Property DepartmentName() As String
        Get
            Return _departmentName
        End Get
        Set(ByVal value As String)
            _departmentName = value
        End Set
    End Property


    Public Property CategoryID() As Integer
        Get
            Return _categoryID
        End Get
        Set(ByVal value As Integer)
            _categoryID = value
        End Set
    End Property


    Public Property Category() As String
        Get
            Return _categoryName
        End Get
        Set(ByVal value As String)
            _categoryName = value
        End Set
    End Property


    Public Property GoalDirection() As String
        Get
            Return _goalDirection
        End Get
        Set(ByVal value As String)
            _goalDirection = value
        End Set
    End Property


    Public Property GoalDate() As String
        Get
            Return _goalDate
        End Get
        Set(ByVal value As String)
            _goalDate = value
        End Set
    End Property


End Class
