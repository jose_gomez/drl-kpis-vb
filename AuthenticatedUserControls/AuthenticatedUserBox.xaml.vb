﻿
Public Class AuthenticatedUserBox
    Private _UserName As String
    Public Event btnSalirClick As RoutedEventHandler

    Public Property UserName() As String
        Get
            Return lblName.Content
        End Get
        Set(ByVal value As String)
            _UserName = value
            lblName.Content = _UserName
        End Set
    End Property

    Private _position As String
    Public Property Position() As String
        Get
            Return lblPosition.Content
        End Get
        Set(ByVal value As String)
            _position = value
            lblPosition.Content = _position
        End Set
    End Property

    Private _currentDate As String
    Public Property CurrentDate() As String
        Get
            Return _currentDate
        End Get
        Set(ByVal value As String)
            _currentDate = value
            lblUSemana.Content = CurrentDate
        End Set
    End Property


    Private _picture As BitmapImage
    Public Property Picture() As BitmapImage
        Get
            Return _picture
        End Get
        Set(ByVal value As BitmapImage)
            _picture = value
            Me.imgUser.Source = Me.Picture
        End Set
    End Property


    Private Sub btnSalir_Click(sender As Object, e As RoutedEventArgs) Handles btnSalir.Click
        If Not btnSalir Is Nothing Then
            RaiseEvent btnSalirClick(sender, e)
        End If
    End Sub

  
End Class
