﻿Imports System.Collections.ObjectModel
Imports System.Dynamic
Imports System.Data

Public Class AgregarMetas
    Dim selectedPeriod As IntervalPeriods
    Dim KpiList As New List(Of Metas)
    Dim rowHeaders As New Dictionary(Of String, Integer)
    Dim dt As New DataTable
    Dim lastestRowAdded As String
    Dim KpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
    Dim employees As New MSEmployeeCollection

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        UIElementsDataHelper.FillDepartmentsWithKPIsCombobox(Me.CboDepartamento)
        'UIElementsDataHelper.FillCategoriesCombobox(Me.cboCategoría)
    
    End Sub

    Public Sub New()
        Try
            InitializeComponent()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        ' This call is required by the designer.


        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Private Sub btnSave_Click(sender As Object, e As RoutedEventArgs) Handles btnSave.Click
        Dim startDate As DateTime, rowIndex As Integer
        Dim endDate As DateTime, kpiID As Integer

        If ValidateData() Then
            Dim departmentID As Integer = CboDepartamento.SelectedValue, GoalName As String

            For Each item As DataRowView In KPIData.Items
                Dim definedGoal As New DefinedGoal

                rowIndex = CType(Me.KPIData.ItemContainerGenerator.ContainerFromItem(item), DataGridRow).GetIndex()
                GoalName = CType(CType(Me.KPIData.ItemContainerGenerator.ContainerFromItem(item), DataGridRow).Header,  _
                       Label).Content.ToString().Replace("Metas:", "").Trim()
                If GoalName = "Dia:" Then
                    Continue For
                End If
                Dim deptID As Integer = CboDepartamento.SelectedValue
                kpiID = (From k In KpiContext.KPIs Where k.KPIName = GoalName And k.DepartmentID = deptID Select k.ID).First()

                definedGoal.StartDate = dteFrom.SelectedDate
                definedGoal.EndDate = dteTo.SelectedDate
                definedGoal.GoalName = GoalName
                definedGoal.KPIID = kpiID
                definedGoal.Responsible = CType(cboResponsable.SelectedItem, Employee).ID
                definedGoal.CreatedBy = UserUtility.GetLoggedOnUser().UserID
                definedGoal.DepartmentID = CboDepartamento.SelectedValue
                KpiContext.DefinedGoals.InsertOnSubmit(definedGoal)
                KpiContext.SubmitChanges()
                For Each col As DataColumn In dt.Columns
                    startDate = DateTime.Parse(col.Caption)
                    endDate = startDate
                    Dim goal As New Goal
                    Dim IsGoalAlreadySaved = (From g In KpiContext.Goals _
                                               Where g.StartDate = startDate And g.departmentID = departmentID And g.GoalName = GoalName Select g).FirstOrDefault()
                    If Not IsGoalAlreadySaved Is Nothing Then
                        goal = IsGoalAlreadySaved
                    End If


                    goal.GoalName = GoalName
                    goal.StartDate = startDate
                    goal.EndDate = endDate
                    goal.DefinedGoalID = definedGoal.ID
                    goal.ExpectedGoalValue = CType(item(col.Caption), Decimal)
                    goal.KPIID = kpiID
                    goal.DefinedBy = UserUtility.GetLoggedOnUser().UserID
                    goal.IsClosed = False
                    goal.WeekNumber = CalendarUtil.GetIso8601WeekOfYear(startDate)
                    goal.departmentID = CboDepartamento.SelectedValue
                    goal.FrequencyID = If(rbdDiario.IsChecked, IntervalPeriods.Diario, IntervalPeriods.Semanal)
                    goal.MatchingValueCriteriaID = rowHeaders.Item(GoalName)
                    goal.AchievedValue = 0
                    goal.EmployeeID = 1
                    KpiContext.Goals.InsertOnSubmit(goal)
                Next
            Next
            KpiContext.SubmitChanges()
            MessageBox.Show("Metas definidas",
                            Me.Title,
                            MessageBoxButton.OK,
                            MessageBoxImage.Information)
            dt.Columns.Clear()
            dt.Rows.Clear()
            rowHeaders.Clear()
            KPIData.Columns.Clear()
            dteFrom.SelectedDate = Nothing
            dteTo.SelectedDate = Nothing
            CboDepartamento.SelectedIndex = -1
            cboKPI.SelectedIndex = -1

        End If
    End Sub

    Private Function ValidateData() As Boolean
        If Not CboDepartamento.SelectedValue Is Nothing Then
            If dteFrom.SelectedDate Is Nothing Or dteTo.SelectedDate Is Nothing Then
                MessageBox.Show("Por favor seleccionar un rango de fechas valido ",
                                                                Me.Title,
                                                                MessageBoxButton.OK,
                                                                MessageBoxImage.Exclamation)
                Return False
            End If


            For Each row As DataRow In dt.Rows
                For Each col As DataColumn In dt.Columns
                    If Not row(col) Is Nothing And Not IsDBNull(row(col)) Then
                    
                    Else
                        row(col) = 0
                    End If
                Next
            Next

            Return True
        Else
            MessageBox.Show("Por favor seleccione el departamento",
                            Me.Title,
                            MessageBoxButton.OK,
                            MessageBoxImage.Exclamation)
            Return False
        End If
    End Function

    Private Sub FormatPeriodDisplay(clearColumns As Boolean)
        If dteFrom Is Nothing Or dteTo Is Nothing Then Exit Sub
        Dim minDate As DateTime = DateTime.Now
        Dim maxDate As DateTime = New DateTime(DateTime.Now.Year, 12, 31)
        If Not dteFrom.SelectedDate Is Nothing Then minDate = dteFrom.SelectedDate
        If Not dteTo.SelectedDate Is Nothing Then maxDate = dteTo.SelectedDate

        If Not dteFrom.SelectedDate Is Nothing And Not dteTo.SelectedDate Is Nothing Then
            Select Case selectedPeriod
                Case IntervalPeriods.Diario
                    PopulateGrid(minDate, maxDate, clearColumns)
                    Me.GridSemanas.Visibility = Windows.Visibility.Hidden
                Case IntervalPeriods.Semanal
                    Dim minWeek As Integer = CalendarUtil.GetIso8601WeekOfYear(minDate)
                    Dim maxWeek As Integer = CalendarUtil.GetIso8601WeekOfYear(maxDate)
                    cboSemana.Items.Clear()
                    For i As Integer = minWeek To maxWeek
                        cboSemana.Items.Add(i)
                    Next
                    cboSemana.SelectedIndex = 0
                    DisplaySemana(cboSemana.SelectedItem)
                    Me.GridSemanas.Visibility = Windows.Visibility.Visible
                Case IntervalPeriods.Mensual
                    '  Me.Scheduler.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Month
            End Select
        End If
    End Sub
    Private Sub DisplaySemana(weekNumber As Integer)

        Dim startDate As DateTime
        Dim endDate As DateTime
        startDate = CalendarUtil.FirstDateOfWeekISO860(DateTime.Now.Year, weekNumber)
        endDate = startDate.AddDays(6)
        If startDate < dteFrom.SelectedDate Then
            startDate = dteFrom.SelectedDate
        End If
        If endDate > dteTo.SelectedDate Then
            endDate = dteTo.SelectedDate
        End If
        PopulateGrid(startDate, endDate, False)

    End Sub

    Private Sub PopulateGrid(startDate As DateTime, endDate As DateTime, clearColumns As Boolean)
        Dim dr As DataRow = dt.NewRow
        If dt.Rows.Count = 0 Then
            dt.Rows.Add(dr)
            rowHeaders.Add("Dia:", 0)
        End If
        Dim currentDate As DateTime
        Me.KPIData.Columns.Clear()
        If clearColumns Then
            dt.Columns.Clear()
        End If

        While startDate <= endDate
            currentDate = New DateTime(DateTime.Now.Year, 1, 1).AddDays(startDate.DayOfYear - 1)
            If Not dt.Columns.Contains(startDate.ToShortDateString().Replace("/", "-")) Then
                dt.Columns.Add(startDate.ToShortDateString().Replace("/", "-"), System.Type.GetType("System.String"))

                dt.Rows(0)(startDate.ToShortDateString().Replace("/", "-")) = startDate.ToString("dddd")
            End If
            Me.KPIData.Columns.Add(New DataGridTextColumn() _
                                   With {.Header = startDate.ToShortDateString().Replace("/", "-"),
                                     .Binding = New Binding(startDate.ToShortDateString().Replace("/", "-"))})
            Me.KPIData.CanUserAddRows = False
            startDate = startDate.AddDays(1)
        End While
        '  Next

        Me.KPIData.ItemsSource = dt.AsDataView()
    End Sub
    Private Sub dteTo_SelectedDateChanged(sender As Object, e As SelectionChangedEventArgs) Handles dteTo.SelectedDateChanged
        If Not dteFrom.SelectedDate Is Nothing And Not dteTo.SelectedDate Is Nothing Then
            If dteTo.SelectedDate < dteFrom.SelectedDate Then
                MessageBox.Show("La fecha final debe de ser menor que la inicial",
                                Me.Title,
                                MessageBoxButton.OK,
                                MessageBoxImage.Exclamation)
                dteTo.SelectedDate = Nothing
                Exit Sub
            End If
            FormatPeriodDisplay(True)
            Dim goals = DataLayer.GetGoalsForDepartmentByDates(CboDepartamento.SelectedValue, dteFrom.SelectedDate, _
                                                               dteTo.SelectedDate)
            If Not goals Is Nothing And goals.Count > 0 Then
                pivotGrid.DataSource = goals
                pivotGrid.ShowColumnGrandTotals = False
            End If
        Else
            If Not dteTo.SelectedDate Is Nothing Then
                MessageBox.Show("Por favor seleccione las fecha de inicio y final",
                                Me.Title,
                                MessageBoxButton.OK,
                                MessageBoxImage.Exclamation)
                dteTo.SelectedDate = Nothing
            End If
        End If
    End Sub

    Private Sub rbdDiario_Checked(sender As Object, e As RoutedEventArgs) Handles rbdDiario.Checked
        If rbdDiario.IsChecked Then
            selectedPeriod = IntervalPeriods.Diario
            FormatPeriodDisplay(False)
            If Me.GridSemanas Is Nothing Then Exit Sub
            Me.GridSemanas.Visibility = Windows.Visibility.Hidden
        End If
    End Sub

    Private Sub rdbSemanal_Checked(sender As Object, e As RoutedEventArgs) Handles rdbSemanal.Checked
        If rdbSemanal.IsChecked Then
            selectedPeriod = IntervalPeriods.Semanal
            FormatPeriodDisplay(False)
            If Me.GridSemanas Is Nothing Then Exit Sub
            Me.GridSemanas.Visibility = Windows.Visibility.Visible
        End If
    End Sub

    Private Sub rdbMensual_Checked(sender As Object, e As RoutedEventArgs) Handles rdbMensual.Checked
        If rdbMensual.IsChecked Then
            selectedPeriod = IntervalPeriods.Mensual
            FormatPeriodDisplay(False)
            If Me.GridSemanas Is Nothing Then Exit Sub
            Me.GridSemanas.Visibility = Windows.Visibility.Hidden
        End If
    End Sub


    Private Sub dteFrom_CalendarOpened(sender As Object, e As RoutedEventArgs) Handles dteFrom.CalendarOpened
        Select Case selectedPeriod
            Case IntervalPeriods.Diario

            Case IntervalPeriods.Semanal
                '  Me.Scheduler.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Week
            Case IntervalPeriods.Mensual
                '  Me.Scheduler.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Month
        End Select
    End Sub

    Private Sub KPIData_LoadingRow(sender As Object, e As DataGridRowEventArgs) Handles KPIData.LoadingRow

        If e.Row.GetIndex() = 0 Then
            e.Row.IsEnabled = False
            e.Row.Header = New Label() With {.Content = "Dia:"}
        Else
            e.Row.Header = New Label() With {.Content = "Metas: " + rowHeaders.Keys(e.Row.GetIndex()).ToString()}
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As RoutedEventArgs) Handles btnAdd.Click
        Dim direction As Integer
        If rdbMaxima.IsChecked = True Then
            direction = GoalDirections.Top
        Else
            direction = GoalDirections.Bottom
        End If
        If Not String.IsNullOrEmpty(cboKPI.Text) And Not dteFrom.SelectedDate Is Nothing _
                    And Not dteTo.SelectedDate Is Nothing Then
            If rowHeaders.Where(Function(s) s.Key = cboKPI.Text).ToList().Count > 0 Then
                MessageBox.Show("Este KPI ya esta en la lista",
                                Me.Title,
                                MessageBoxButton.OK,
                                 MessageBoxImage.Exclamation)
                Exit Sub
            End If
            Dim dr As DataRow
            dr = dt.NewRow

            For Each c As DataColumn In dt.Columns

                dr(c) = 0
            Next
            dt.Rows.Add(dr)
            rowHeaders.Add(cboKPI.Text.Trim(), direction)
            KPIData.ItemsSource = dt.AsDataView()

        Else
            MessageBox.Show("Por favor seleccionar un KPI e ingresar el rango de fechas correspondiente",
                            "Agregar Metas",
                            MessageBoxButton.OK,
                            MessageBoxImage.Exclamation)
        End If
    End Sub

    Private Sub chkApplyToLine_Click(sender As Object, e As RoutedEventArgs) Handles chkApplyToLine.Click
        If Not KPIData.SelectedItem Is Nothing Then
            If ApplyToLine.Value = 0 Then
                MessageBox.Show("Por favor ingresar un valor mayor a 0 para aplicar a la linea",
                                Me.Title,
                               MessageBoxButton.OK,
                               MessageBoxImage.Exclamation)
                Exit Sub
            End If
            Dim row = dt.Rows(KPIData.Items.IndexOf(KPIData.SelectedItem))
            For Each c As DataGridTextColumn In KPIData.Columns
                row(c.Header) = ApplyToLine.Value
            Next
            ' For i = 0 To dt.Columns.Count - 1
            'row(i) = ApplyToLine.Value
            ' Next
        End If
    End Sub

    Private Sub cboSemana_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles cboSemana.SelectionChanged
        If Not cboSemana.SelectedItem Is Nothing Then
            DisplaySemana(cboSemana.SelectedItem)
        End If
    End Sub

    Private Sub dteFrom_SelectedDateChanged(sender As Object, e As SelectionChangedEventArgs) Handles dteFrom.SelectedDateChanged
        If Not dteTo.SelectedDate Is Nothing Then
            If dteTo.SelectedDate < dteFrom.SelectedDate Then
                MessageBox.Show("La fecha final debe de ser menor que la inicial",
                                Me.Title,
                                MessageBoxButton.OK,
                                MessageBoxImage.Exclamation)
                dteFrom.SelectedDate = Nothing
                Exit Sub
            End If
            FormatPeriodDisplay(True)
        End If
    End Sub

    Private Sub CboDepartamento_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles CboDepartamento.SelectionChanged
        If Not CboDepartamento.SelectedValue Is Nothing Then
            UIElementsDataHelper.FillKPIComboboxByDepartment(Me.cboKPI, CboDepartamento.SelectedValue)
            employees = DataLayer.GetEmployees(StringUtilities.UnAccent(CType(CboDepartamento.SelectedItem, Department).DepartmentName))
            Me.DataContext = employees
            If cboKPI.Items.Count = 0 Then
                MessageBox.Show("Este departamento no ha definido ningun KPI. Por favor definir uno e intentar de nuevo",
                                Me.Title,
                                MessageBoxButton.OK,
                                MessageBoxImage.Exclamation)
            End If
        End If
    End Sub

End Class
