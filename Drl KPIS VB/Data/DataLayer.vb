﻿Public Class DataLayer

    Public Shared Function GetGoalsForDepartmentByDates(departmentId As Integer,
                                                        startdate As DateTime,
                                                        endDate As DateTime,
                                                        kpiID As Integer) As  _
        List(Of KPIsViewModel)

        Dim Goals = GetGoalsForDepartmentByDates(departmentId, startdate, endDate).Where(Function(g) g.ID = kpiID).ToList()
        Return Goals
    End Function


    Public Shared Function GetDepartmentName(deptID As Integer) As String
        Dim KpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim deptName As String = (From d In KpiContext.Departments Where d.ID = deptID Select d.DepartmentName).First()
        Return deptName
    End Function



    Public Shared Function GetWeeklyKPIS(theDate As DateTime, dept As Integer) As List(Of WeeklyGoalsReportItem)
        Dim KpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim weekNumber As Integer = CalendarUtil.GetIso8601WeekOfYear(theDate)
        Dim weekStart As DateTime = CalendarUtil.FirstDateOfWeekISO860(theDate.Year, weekNumber)
        Dim weekEnd As DateTime = weekStart.AddDays(6)
        Dim retVal As New List(Of WeeklyGoalsReportItem)
        Dim kps = (From k In KpiContext.SP_WeeklyGoalsByDept(weekStart, weekEnd, dept) Select New WeeklyGoalsReportItem() _
                   With {.Monday = CleanNull(k.Monday), .Tuesday = CleanNull(k.Tuesday), .Wednesday = CleanNull(k.Wednesday), .Thursday = CleanNull(k.Thursday),
                        .Friday = CleanNull(k.Friday), .Saturday = CleanNull(k.Saturday), .Sunday = CleanNull(k.Sunday),
                         .MatchingValueCriteriaID = CleanNull(k.MatchingValueCriteriaID),
                        .GoalName = CleanNull(k.GoalName), .Goal = CleanNull(k.Goal), .DepartmentID = k.departmentID, .DefinedGoalID = k.DefinedGoalID}).ToList()
        'Dim kps = (From k In KpiContext.SP_WeeklyGoalsByDept(weekStart, weekEnd, dept) Select
        '            k.Monday, k.Tuesday, k.Wednesday, k.Thursday,
        '               k.Friday, k.Saturday, k.Sunday, k.MatchingValueCriteriaID,
        '               k.GoalName, k.Goal).ToList()

        'For Each item In kps
        '    Dim weeklyGoal As New WeeklyGoalsReportItem
        '    weeklyGoal.Monday = CleanNull(item.Monday)

        'Next
        Return kps
    End Function

    Public Shared Function CleanNull(theVal As Object) As Object
        If theVal Is Nothing Then
            Return 0
        Else
            Return theVal
        End If
    End Function

    Public Shared Function GetGoalsRequiringAttention() As List(Of SP_GoalsRequiringAttentionResult)
        Dim KpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim result = (From r In KpiContext.SP_GoalsRequiringAttention Select r).ToList()
        Return result
    End Function

    Public Shared Function GetEmployeeByID(numero_empleado As Integer) As MaestrodeEmpleado
        Dim payrollContext As New PayrollDataContext(ConnectionStringsHelper.GetPayrollConnectionString)
        Dim emp = (From em In payrollContext.MaestrodeEmpleados.Where(Function(x) x.Numero_empleado = numero_empleado) Select em).FirstOrDefault()
        Return emp
    End Function

    Public Shared Function GetGoalsForDepartmentByDates(departmentId As Integer,
                                                     startdate As DateTime,
                                                     endDate As DateTime) As  _
     List(Of KPIsViewModel)

        Dim KpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim Goals = (From goal In KpiContext.Goals Where goal.departmentID = departmentId And _
           goal.StartDate >= startdate And goal.EndDate <= endDate Select New KPIsViewModel _
           With {.Category = goal.KPI.Category.CategoryName, .DepartmentID = goal.departmentID, _
                .DepartmentName = goal.Department.DepartmentName, .GoalDate = goal.StartDate, _
                .DesiredGoal = goal.ExpectedGoalValue, .GoalDirection = goal.MatchingValueCriteriaID, _
                 .KPIName = goal.KPI.KPIName, .AchievedValue = goal.AchievedValue,
                 .DefinedGoalID = goal.DefinedGoalID, .ID = goal.KPIID, .GoalID = goal.ID, .IsClosed = goal.IsClosed, .ResponsibleID = goal.DefinedGoal.Responsible}).ToList()
        Return Goals
    End Function

    Public Shared Function GetEmployees(dept As String) As MSEmployeeCollection
        Dim payrollContext As New PayrollDataContext(ConnectionStringsHelper.GetPayrollConnectionString)
        '   payrollContext.Log = Console.Out
        Dim emps = (From e In payrollContext.MaestrodeEmpleados Join f In payrollContext.tblFotos
                    On e.Numero_empleado Equals f.Numero_Empleado Where e.Departamento = dept And e.Estatus = "A" Select e.Nombre_empleado, f.Fotos, e.Apellido_empleado,
                    FullName = e.Nombre_empleado + " " + e.Nombre_empleado, e.Numero_empleado).ToList()

        Dim ms As New MSEmployeeCollection
        For Each emp In emps
            Dim employee As New Employee
            employee.FirstName = emp.Nombre_empleado
            employee.LastName = emp.Apellido_empleado
            employee.FullName = emp.FullName
            employee.PictureByteArray = emp.Fotos.ToArray()
            employee.ID = emp.Numero_empleado
            ms.Add(employee)
        Next
        Return ms
    End Function

    Public Shared Function GetAverageForPeriod(startDate As DateTime, endDate As DateTime, goalName As String, dept As Integer) As Double
        Dim kpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim averageList = (From k In kpiContext.Goals Where k.departmentID = dept And k.GoalName = goalName _
                      And k.StartDate >= startDate And k.EndDate <= endDate _
                                   Select k.AchievedValue).ToList()
        Dim average As Decimal = 0
        If averageList.Count > 0 Then

            average = averageList.Average()

        End If
        Return average
    End Function

    ''' <summary>
    ''' Retorna el promedio de los avances para una meta y un departamento
    ''' </summary>
    ''' <param name="startDate">Fecha de inicio</param>
    ''' <param name="endDate">Fecha Final</param>
    ''' <param name="goalName">Meta a calcular</param>
    ''' <param name="dept">ID del departamento</param>
    ''' <returns>El promedio para el periodo comprendido entre la fecha de inicio y la fecha final</returns>
    ''' <remarks></remarks>
    Public Shared Function GetSumForPeriod(startDate As DateTime, endDate As DateTime, goalName As String, dept As Integer) As Double
        Dim kpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim averageList = (From k In kpiContext.Goals Where k.departmentID = dept And k.GoalName = goalName _
                      And k.StartDate >= startDate And k.EndDate <= endDate _
                                   Select k.AchievedValue).ToList()
        Dim average As Decimal = 0
        If averageList.Count > 0 Then

            average = averageList.Sum()

        End If
        Return average
    End Function

    Public Shared Function GetGoalAverageForDepartment(startDate As DateTime, endDate As DateTime, dept As Integer) As Double
        Dim kpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim averageList = (From k In kpiContext.Goals Where k.departmentID = dept _
                      And k.StartDate >= startDate And k.EndDate <= endDate _
                                   Select k.AchievedValue, k.ExpectedGoalValue).ToList()
        Dim average As Decimal = 0
        If averageList.Count > 0 Then

            average = (From k In averageList Select k.AchievedValue).Sum() / (From k In averageList Select k.ExpectedGoalValue).Sum()

        End If
        Return average
    End Function

    Public Shared Function GetGoalAverageForPeriod(startDate As DateTime, endDate As DateTime, goalName As String, dept As Integer) As Double
        Dim kpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim averageList = (From k In kpiContext.Goals Where k.departmentID = dept And k.GoalName = goalName _
                      And k.StartDate >= startDate And k.EndDate <= endDate _
                                   Select k.ExpectedGoalValue).ToList()
        Dim average As Decimal = 0
        If averageList.Count > 0 Then

            average = averageList.Average()

        End If
        Return average
    End Function

    Public Shared Sub SaveGoalsProgress(metas As List(Of KPIsViewModel))
        Dim KpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim user As User = UserUtility.GetLoggedOnUser()

        For Each kpi As KPIsViewModel In metas
            Dim goal = (From g In KpiContext.Goals Where g.ID = kpi.GoalID Select g).First()
            goal.ID = kpi.GoalID
            goal.AchievedValue = kpi.AchievedValue
            goal.SavedOn = DateTime.Now
            goal.SavedBy = user.UserID
            goal.IsClosed = kpi.IsClosed
            KpiContext.SubmitChanges()
        Next
    End Sub

    Public Shared Sub SaveActionPlans(plans As List(Of ActionPlan))
        Dim KpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        For Each pl As ActionPlan In plans
            Dim plan As ActionPlan = (From p In KpiContext.ActionPlans Where p.ID = pl.ID Select p).FirstOrDefault()
            If plan Is Nothing Then
                KpiContext.ActionPlans.InsertOnSubmit(pl)
                KpiContext.SubmitChanges()
            Else
                plan.Responsible = pl.Responsible
                plan.ComplianceDate = pl.ComplianceDate
                plan.Notes = pl.Notes
                plan.Situation = pl.Situation
                plan.PriorityID = pl.PriorityID
                plan.CorrectiveAction = pl.CorrectiveAction
                plan.Progress = pl.Progress
                plan.ReponsibleEmployeeID = pl.ReponsibleEmployeeID
                KpiContext.SubmitChanges()

            End If
        Next
    End Sub

    Public Shared Function GetActionPlanForDefinedGoal(definedGoalID As Integer) As List(Of ActionPlanViewModel)
        Dim KpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())

        Dim plans = (From pl In KpiContext.ActionPlans Join gl In KpiContext.Goals On pl.GoalID Equals gl.ID
                     Join dgl In KpiContext.DefinedGoals On gl.DefinedGoalID Equals dgl.ID
                     Where dgl.ID = definedGoalID
                     Select New ActionPlanViewModel _
                     With {.ID = pl.ID, .GoalID = pl.GoalID, .ProgresoNumericValue = pl.Progress,
                          .CorrectiveAction = pl.CorrectiveAction, .ComplianceDate = pl.ComplianceDate,
                          .PlanDate = gl.StartDate, .KPIName = gl.GoalName, .Notes = pl.Notes,
                          .KPIID = gl.KPIID, .ResponsibleID = dgl.Responsible, .SelectedPriorityLevel = pl.PriorityID,
                          .Situation = pl.Situation, .DefinedGoalID = dgl.ID}).ToList()

        Return plans
    End Function
End Class
