﻿Public Class PriorityLevelConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Select Case value
            Case ActionPlanPriorityLevels.Alta
                Return "Alta"
            Case ActionPlanPriorityLevels.Media
                Return "Media"
            Case ActionPlanPriorityLevels.Baja '
                Return "Baja"
            Case Else
                Return ActionPlanPriorityLevels.Baja
        End Select
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Select Case value
            Case "1"
                Return ActionPlanPriorityLevels.Alta
            Case "2"
                Return ActionPlanPriorityLevels.Media
            Case "3"
                Return ActionPlanPriorityLevels.Baja
            Case Else
                Return ActionPlanPriorityLevels.Baja
        End Select
    End Function
End Class
