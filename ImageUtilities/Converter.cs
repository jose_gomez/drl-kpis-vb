﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace ImageUtilities
{
    public class Converter
    {
        public Bitmap ConvertImage(object pic)
        {
            byte[] imgData = (byte[])pic;
            if (imgData.Length > 0)
            {
                try
                {
                    MemoryStream memStream = new MemoryStream(imgData, true);
                    OleStripper stripper = new OleStripper(memStream);
                    Stream strippedStream = stripper.GetStrippedStream();
                    Bitmap bp = new Bitmap(strippedStream);
                    return bp;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            return null;
        }



    }
}
