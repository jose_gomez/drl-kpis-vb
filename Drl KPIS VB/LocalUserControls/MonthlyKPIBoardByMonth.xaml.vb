﻿
Imports System.Collections.ObjectModel
Imports System.Globalization

Public Class MonthlyKPIBoardByMonth
    Dim indicadores As New ObservableCollection(Of KPIsViewModel)

    Private _currentMonth As DateTime
    Private _currentDept As Integer

    Public Property CurrentMonth() As DateTime
        Get
            Return _currentMonth
        End Get
        Set(value As DateTime)
            _currentMonth = value
        End Set
    End Property


    Public Property CurrentDept As Integer
        Get
            Return _currentDept
        End Get
        Set(value As Integer)
            _currentDept = value
        End Set
    End Property

    Public Sub New(month As DateTime, dept As Integer)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._currentDept = dept
        Me._currentMonth = month



    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'Me._currentMonth = DateTime.Now
        'Me._currentDept = 23
        'CalculateColumns()

    End Sub

    Public Sub CalculateColumns()
        Dim weeks As List(Of Integer) = CalendarUtil.GetWeeksInMonth(Me.CurrentMonth)
        For Each ind As Integer In weeks
            Me.grdIndicatorBoard.Columns.Add(New DevExpress.Xpf.Grid.GridColumn() With {.Header ="WK" + ind.ToString(),
                                                                                        .UnboundType = DevExpress.Data.UnboundColumnType.String,
                                                                                        .FieldName = "WK" + ind.ToString(), .ReadOnly = True})
        Next
        Dim theColumn As DevExpress.Xpf.Grid.GridColumnBase = New DevExpress.Xpf.Grid.GridColumn() With {.Header = _currentMonth.ToString("MMM", CultureInfo.InvariantCulture),
                                                                                    .UnboundType = DevExpress.Data.UnboundColumnType.String,
                                                                                    .FieldName = "MTD", .ReadOnly = True, .AllowEditing = DevExpress.Utils.DefaultBoolean.False}
 

        Me.grdIndicatorBoard.Columns.Add(theColumn)
        Me.grdIndicatorBoard.Columns.Add(New DevExpress.Xpf.Grid.GridColumn() With {.Header = "YTD",
                                                                                    .UnboundType = DevExpress.Data.UnboundColumnType.String,
                                                                                    .FieldName = "YTD", .ReadOnly = True})

        lblCurrentMonth.Content = _currentMonth.ToString("MMMM", CultureInfo.InvariantCulture)
    End Sub
    Public Sub ShowReport()



        If Me.CurrentDept = 0 Then Exit Sub
        CalculateColumns()
        Dim kpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim payrollContext As New PayrollDataContext(ConnectionStringsHelper.GetPayrollConnectionString())
        Dim startDate As DateTime = CalendarUtil.FirstDayOfMonth(Me._currentMonth)
        Dim endDate As DateTime = New DateTime(startDate.Year, startDate.Month, DateTime.DaysInMonth(startDate.Year, startDate.Month))
        Dim yearStart As DateTime = New DateTime(DateTime.Now.Year, 1, 1)
        Dim yearEnd As DateTime = New DateTime(DateTime.Now.Year, 12, 31)
        If Me._currentDept > 0 Then
            Dim deptName As String = (From d In kpiContext.Departments Where d.ID = _currentDept Select d.DepartmentName).FirstOrDefault()
            lblDepartment.Content = deptName
        End If
        Dim indicators = (From k In kpiContext.SP_KPIsByMonth(startDate, endDate).AsEnumerable() Join emp In payrollContext.MaestrodeEmpleados.AsEnumerable()
          On k.Responsible Equals emp.Numero_empleado Where k.DepartmentID = _currentDept
            Select New KPIsViewModel() With {.Category = k.CategoryName, .KPIName = k.KPIName, .FullName = emp.Nombre_empleado + " " + emp.Apellido_empleado, _
         .PreviousPeriod = k.PreviousPeriod, .DesiredGoal = k.Meta, .GoalDirection = k.MatchingValueCriteriaID, .AchievedValue = k.AchievedValue}).ToList()

        For Each s In indicators
            Dim x = s.Background
        Next
        Me.lblMonthToDateValue.Content = (DataLayer.GetGoalAverageForDepartment(startDate, endDate, CurrentDept)).ToString("P")
        Me.lblYTDValue.Content = (DataLayer.GetGoalAverageForDepartment(yearStart, yearEnd, CurrentDept)).ToString("P")
        Me.grdIndicatorBoard.ItemsSource = indicators


    End Sub





    Private Sub TableView_InitNewRow(sender As Object, e As DevExpress.Xpf.Grid.InitNewRowEventArgs)
        Dim x = e.RowHandle

    End Sub

    Private Sub grdIndicatorBoard_CustomUnboundColumnData(sender As Object, e As DevExpress.Xpf.Grid.GridColumnDataEventArgs)
        Dim kpiContext As New KPIDataContextDataContext(ConnectionStringsHelper.GetKPIDBConnectionString())
        Dim col As String = e.Column.HeaderCaption.ToString()
        Dim weekSum As Double = 0
        Dim yearToDateAvg As Double = 0
        Dim monthToDateavg As Double

        Dim goalName As String
        goalName = e.Source.ItemsSource(e.ListSourceRowIndex).KPIName

        Dim yearStart As DateTime = New DateTime(DateTime.Now.Year, 1, 1)
        Dim yearEnd As DateTime = New DateTime(DateTime.Now.Year, 12, 31)
        Dim monthStart As DateTime = New DateTime(_currentMonth.Year, _currentMonth.Month, 1)
        Dim monthEnd As DateTime = New DateTime(_currentMonth.Year, _currentMonth.Month, DateTime.DaysInMonth(_currentMonth.Year, _currentMonth.Month))


        If col.Contains("WK") Then
            Dim weekStart As DateTime = CalendarUtil.FirstDateOfWeekISO860(_currentMonth.Year, Integer.Parse(col.Trim().Replace("WK", "")))
            Dim weekEnd As DateTime = weekStart.AddDays(6)

            Dim dept As Integer = _currentDept
            weekSum = DataLayer.GetSumForPeriod(weekStart, weekEnd, goalName, _currentDept)

            If weekSum > 0 Then
                weekSum = weekSum
            End If
            e.Value = (weekSum).ToString("F2")

        End If
        If col.Contains(_currentMonth.ToString("MMM", CultureInfo.InvariantCulture)) Then
        



            Dim monthToDateGoalAvg As Double = DataLayer.GetGoalAverageForPeriod(monthStart, monthEnd, goalName, _currentDept)
            monthToDateavg = DataLayer.GetAverageForPeriod(monthStart, monthEnd, goalName, _currentDept)
            If monthToDateavg > 0 Then
                monthToDateavg = monthToDateavg / monthToDateGoalAvg
                e.Value = (monthToDateavg).ToString("P2")
            End If
        End If

        If col.Contains("YTD") Then
            Dim yearToDateGoalsAvg As Double = DataLayer.GetGoalAverageForPeriod(yearStart, yearEnd, goalName, _currentDept)
            yearToDateAvg = DataLayer.GetAverageForPeriod(yearStart, yearEnd, goalName, _currentDept)
            If yearToDateAvg > 0 Then
                e.Value = ((yearToDateAvg / yearToDateGoalsAvg)).ToString("P")
            End If
        End If

    End Sub
End Class
