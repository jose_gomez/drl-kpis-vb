﻿Imports System.Configuration
Public Class ConnectionStringsHelper

    Public Shared Function GetKPIDBConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("KeyPerformanceIndicatorsConnectionString").ConnectionString
    End Function

    Public Shared Function GetJTSDBConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("JTS_DRConnectionString").ConnectionString
    End Function

    Public Shared Function GetPayrollConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("PayrollConnectionString").ConnectionString
    End Function
End Class
