﻿Public Class Metas

    Private _category As String
    Private _kpi As String
    Private _goalValue As Decimal
    Private _achievedValue As Decimal
    Private _goalDate As DateTime

    Public Property Category() As String
        Get
            Return _category
        End Get
        Set(ByVal value As String)
            _category = value
        End Set
    End Property


    Public Property KPI() As String
        Get
            Return _kpi
        End Get
        Set(ByVal value As String)
            _kpi = value
        End Set
    End Property


    Public Property GoalValue() As Decimal
        Get
            Return _goalValue
        End Get
        Set(ByVal value As Decimal)
            _goalValue = value
        End Set
    End Property


    Public Property AchievedValue() As String
        Get
            Return _achievedValue
        End Get
        Set(ByVal value As String)
            _achievedValue = value
        End Set
    End Property

    Public Property GoalDate() As DateTime
        Get
            Return _goalDate
        End Get
        Set(ByVal value As DateTime)
            _goalDate = value
        End Set
    End Property



End Class
