﻿Public Class Login
    Private _isLogginSuccess As Boolean

    Public Property IsLogginSuccess() As Boolean
        Get
            Return _isLogginSuccess
        End Get
        Set(ByVal value As Boolean)
            _isLogginSuccess = value
        End Set
    End Property

    Private Sub LoginUser(sender As Object, e As RoutedEventArgs)
        Try
            If Not String.IsNullOrEmpty(ctlLogin.UserName) And Not String.IsNullOrEmpty(ctlLogin.Password) Then
                If JTSPrincipal.IsValidUser(ctlLogin.UserName, ctlLogin.Password) Then
                    Me.IsLogginSuccess = True
                    Me.Close()
                Else
                    MessageBox.Show("Por favor digite el nombre de usuario y el password",
                                    Me.Title,
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Exclamation)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
     
    End Sub


    Private Sub Salir(sender As Object, e As RoutedEventArgs)
        Me.Close()
    End Sub

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        AddHandler ctlLogin.iniciarClick, AddressOf LoginUser
        AddHandler ctlLogin.cancelarClick, AddressOf Salir
    End Sub
End Class
