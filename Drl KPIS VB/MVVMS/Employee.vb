﻿Imports System.IO
Imports System.Drawing.Imaging

Public Class Employee

    Private _fullName As String
    Public Property FullName() As String
        Get
            Return _fullName
        End Get
        Set(ByVal value As String)
            _fullName = value
        End Set
    End Property

    Private _pictureByteArray As Byte()
    Public WriteOnly Property PictureByteArray() As Byte()

        Set(ByVal value As Byte())
            _pictureByteArray = value
            Dim converter As New ImageUtilities.Converter
            Dim img As System.Drawing.Bitmap
            img = converter.ConvertImage(value)
            Dim Image As New BitmapImage
            If Not img Is Nothing Then
                Using memory As New MemoryStream
                    img.Save(memory, ImageFormat.Png)
                    memory.Position = 0
                    Image.BeginInit()
                    Image.StreamSource = memory
                    Image.CacheOption = BitmapCacheOption.OnLoad
                    Image.EndInit()
                End Using
            End If
            Me._picture = Image
        End Set
    End Property


    Private _picture As BitmapImage
    Public ReadOnly Property Picture() As BitmapImage
        Get
            Return _picture
        End Get

    End Property



End Class
